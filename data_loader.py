import h5py
import numpy as np
import random
from time import sleep
import copy

def get_resized_img_dim(w,h,min_side=600):
	if h < w:
		return int((600.*w)/h),600
	else:
		return 600,int((600.*h)/w)

def calc_intersection(ai, bi):
	x = max(ai[0], bi[0])
	y = max(ai[1], bi[1])
	w = min(ai[2], bi[2]) - x
	h = min(ai[3], bi[3]) - y
	if w < 0 or h < 0:
		return 0, 0, 0, 0
	return x, y, w, h

def calc_union(au, bu, intersection):
	area_a = (au[2] - au[0]) * (au[3] - au[1])
	area_b = (bu[2] - bu[0]) * (bu[3] - bu[1])
	area_intersection = intersection[2] * intersection[3]
	area_union = area_a + area_b - area_intersection
	return area_union

# a and b should be (x1,y1,x2,y2)
def calc_iou(a, b):
	if a[0] >= a[2] or a[1] >= a[3] or b[0] >= b[2] or b[1] >= b[3]:
		return 0.0

	i = calc_intersection(a, b)
	area_u = calc_union(a, b, i)
	area_i = i[2] * i[3]

	return float(area_i) / float(area_u)

# params = [(bbox, params)...] i.e. [ ([x,y,x,y],[A,B,C,r,s,t])... ]
# img_res = (input_img_height, input_img_width)
def calc_rpn_labels(params, img_res, anchor_scales, anchor_shapes, out_dim):
	# size of feature maps output from rpn
	out_w, out_h = out_dim

	best_regr = (0,0,0,0)

	MAX_OVERLAP = 0.7
	MIN_OVERLAP = 0.3

	n_regions = 256

	bboxes = np.array(copy.deepcopy(params))[:,0]

	n_bboxes = len(bboxes)
	n_anchor_shapes = len(anchor_shapes)
	n_anchors = len(anchor_scales)*n_anchor_shapes
	scale_x = (float(out_w)/img_res[1])
	scale_y = (float(out_h)/img_res[0])

	# init output
	y_rpn_overlap = np.zeros((out_h, out_w, n_anchors))
	y_box_valid = np.zeros((out_h, out_w, n_anchors))
	y_rpn_regr = np.zeros((out_h, out_w, n_anchors * 4))

	num_anchors_for_bbox = np.zeros(n_bboxes, dtype=int)
	best_anchor_for_bbox = -1*np.ones((n_bboxes, 4), dtype=int)
	best_iou_for_bbox = np.zeros(n_bboxes, dtype=np.float32)
	best_x_for_bbox = np.zeros((n_bboxes, 4), dtype=int)
	best_dx_for_bbox = np.zeros((n_bboxes, 4), dtype=np.float32)

	# scale ground truth boxes down to rpn output size
	for i in xrange(n_bboxes):
		bboxes[i][0] *= scale_x
		bboxes[i][1] *= scale_y
		bboxes[i][2] *= scale_x
		bboxes[i][3] *= scale_y

	# iterate over all anchor types
	for ascale_idx in xrange(len(anchor_scales)):
		for ashape_idx in xrange(n_anchor_shapes):
			anchor_w = anchor_scales[ascale_idx]*anchor_shapes[ashape_idx][0] * scale_x # output is small, of course it's always 
			anchor_h = anchor_scales[ascale_idx]*anchor_shapes[ashape_idx][1] * scale_y # out of bounds w/ large anchor scales

			for i in xrange(out_w):
				# x coords of the current anchor box
				# originally there's a downscale * in front (rpn_stride); no brackets
				anchor_x1 = (i + 0.5) - anchor_w/2.
				anchor_x2 = (i + 0.5) + anchor_w/2.

				# ignore boxes that go across image boundaries					
				if anchor_x1 < 0 or anchor_x2 > out_w:
					continue

				for j in xrange(out_h):
					# y coords of the current anchor box	
					anchor_y1 = (j + 0.5) - anchor_h/2.
					anchor_y2 = (j + 0.5) + anchor_h/2.

					# ignore boxes that go across image boundaries
					if anchor_y1 < 0 or anchor_y2 > out_h:
						continue

					bbox_type = 'neg'
					best_iou_here = 0.0

					for bbox_idx in xrange(n_bboxes):
						bbox_iou = calc_iou(bboxes[bbox_idx], [anchor_x1, anchor_y1, anchor_x2, anchor_y2])

						if bbox_iou > best_iou_for_bbox[bbox_idx] or bbox_iou > MAX_OVERLAP:
							cx = (bboxes[bbox_idx][0] + bboxes[bbox_idx][2]) / 2.
							cy = (bboxes[bbox_idx][1] + bboxes[bbox_idx][3]) / 2.
							cxa = (anchor_x1 + anchor_x2) / 2.
							cya = (anchor_y1 + anchor_y2) / 2.

							# (tx,ty) centroid error vector, proportionate to anchor size
							tx = (cx - cxa) / (anchor_x2 - anchor_x1)
							ty = (cy - cya) / (anchor_y2 - anchor_y1)
							# log relationship between box and anchor size
							tw = np.log((bboxes[bbox_idx][2] - bboxes[bbox_idx][0]) / (anchor_x2 - anchor_x1))
							th = np.log((bboxes[bbox_idx][3] - bboxes[bbox_idx][1]) / (anchor_y2 - anchor_y1))

						# save best anchor box
						if bbox_iou > best_iou_for_bbox[bbox_idx]:
							best_anchor_for_bbox[bbox_idx] = [i, j, ashape_idx, ascale_idx]
							best_iou_for_bbox[bbox_idx] = bbox_iou
							best_x_for_bbox[bbox_idx,:] = [anchor_x1, anchor_x2, anchor_y1, anchor_y2]
							best_dx_for_bbox[bbox_idx,:] = [tx, ty, tw, th]

						# boxes with good overlap are always positive matches (positive overlap)
						if bbox_iou > MAX_OVERLAP:
							bbox_type = 'pos'
							num_anchors_for_bbox[bbox_idx] += 1
							# update rpn regression target
							if bbox_iou > best_iou_here:
								best_iou_here = bbox_iou
								best_regr = (tx, ty, tw, th)

						# ambigous box overlap; neutral overlaps get discarded
						if MIN_OVERLAP < bbox_iou < MAX_OVERLAP:
							if bbox_type != 'pos':
								bbox_type = 'neutral'

					if bbox_type == 'neg':
						y_box_valid  [j, i, ashape_idx + n_anchor_shapes * ascale_idx] = 1
						y_rpn_overlap[j, i, ashape_idx + n_anchor_shapes * ascale_idx] = 0
					elif bbox_type == 'neutral':
						y_box_valid  [j, i, ashape_idx + n_anchor_shapes * ascale_idx] = 0
						y_rpn_overlap[j, i, ashape_idx + n_anchor_shapes * ascale_idx] = 0
					elif bbox_type == 'pos':
						y_box_valid  [j, i, ashape_idx + n_anchor_shapes * ascale_idx] = 1
						y_rpn_overlap[j, i, ashape_idx + n_anchor_shapes * ascale_idx] = 1

						start = 4 * (ashape_idx + n_anchor_shapes * ascale_idx)
						y_rpn_regr[j, i, start:start+4] = best_regr

	# make sure every anchor box has at least 1 positive region
	for i in xrange(num_anchors_for_bbox.shape[0]):
		if num_anchors_for_bbox[i] == 0:
			# no box with an IOU greater than zero
			if best_anchor_for_bbox[i, 0] == -1:
				continue
			y_box_valid[
				best_anchor_for_bbox[i,0], 
				best_anchor_for_bbox[i,1], 
				best_anchor_for_bbox[i,2] + n_anchor_shapes * best_anchor_for_bbox[i,3] ] = 1
			y_rpn_overlap[
				best_anchor_for_bbox[i,0], 
				best_anchor_for_bbox[i,1], 
				best_anchor_for_bbox[i,2] + n_anchor_shapes * best_anchor_for_bbox[i,3] ] = 1

			start = 4 * (best_anchor_for_bbox[i,2] + n_anchor_shapes * best_anchor_for_bbox[i,3])
			y_rpn_regr[
				best_anchor_for_bbox[i,0], 
				best_anchor_for_bbox[i,1], 
				start:start+4 ] = best_dx_for_bbox[i, :]

	# format data to fit keras model
	y_rpn_overlap = np.expand_dims(y_rpn_overlap, axis=0)
	y_box_valid = np.expand_dims(y_box_valid, axis=0)
	y_rpn_regr = np.expand_dims(y_rpn_regr, axis=0)

	pos_locs = np.where(np.logical_and(y_rpn_overlap[0, :, :, :] == 1, y_box_valid[0, :, :, :] == 1))
	neg_locs = np.where(np.logical_and(y_rpn_overlap[0, :, :, :] == 0, y_box_valid[0, :, :, :] == 1))

	n_pos_locs = len(pos_locs[0])

	# rpn has many more neg than pos samples - limit to 256 regions i.e. turn off validity for some regions
	# at most half must be positive, pad with negative if not enough positive
	if len(pos_locs[0]) > n_regions/2:
		val_locs = random.sample(range(len(pos_locs[0])), len(pos_locs[0]) - n_regions/2)
		y_box_valid[0, pos_locs[0][val_locs], pos_locs[1][val_locs], pos_locs[2][val_locs]] = 0
		n_pos_locs = n_regions/2

	if len(neg_locs[0]) + n_pos_locs > n_regions:
		val_locs = random.sample(range(len(neg_locs[0])), len(neg_locs[0]) - n_pos_locs)
		y_box_valid[0, neg_locs[0][val_locs], neg_locs[1][val_locs], neg_locs[2][val_locs]] = 0

	y_rpn_cls = np.multiply(y_box_valid, y_rpn_overlap) 
	y_rpn_regr = np.multiply(np.repeat(y_rpn_overlap, 4, axis=-1), y_rpn_regr) 

	return np.copy(y_rpn_cls), np.copy(y_rpn_regr)

# returns:
# 	imgs array with shape = (n_imgs, h, w, 1)
def load_h5_imgparam_dataset(fname, img_h, img_w):
	with h5py.File(fname, 'r') as hf:
		dataset = hf['scenes'][:]

	print "H5 dataset shape:",dataset.shape
	
	return dataset.reshape(len(dataset), img_h, img_w,1)

# scenes/images in roi labels file _should be_ in the same relative order as scenes/images from load_h5_imgparam_dataset
# returns:
# 	bboxes[ img[ (bbox[x1,y1,x2,y2], params[float...])... ]... ]
def load_bboxes_labels(fname):
	bboxes = []
	with open(fname, "r") as f:
		tmp = []
		curr_img = ""
		for line in f:
			banana = line.split(",")
			if banana[0] != curr_img:
				curr_img = banana[0]
				bboxes.append(tmp)
				tmp = []
			# append the bounding box for the object and the parameters of the object
			tmp.append( ([ int(banana[1]),int(banana[2]),int(banana[3]),int(banana[4]) ], map(float, banana[6:])) )

		# first iteration an additional tmp gets appended and the last tmp(last iteration) does not; fixed here
		bboxes.pop(0)
		bboxes.append(tmp)

	return np.array(bboxes)

# expects data to be shaped like [ ( img[img_h[float...]], [(bbox, params)...] )... ]
# yields next image to train the rpn on and the "label" for the rpn to be able to calc loss
def data_generator(data, anchor_scales, anchor_shapes, stddev_scaling=0.4, out_dim=(13,13)):
	idx = 0
	while True:
		idx %= len(data)
		img = data[idx][0]
		params = data[idx][1]
		try:
			y_rpn_cls,y_rpn_reg = calc_rpn_labels(params, img.shape[:2], anchor_scales, anchor_shapes, out_dim)
		except Exception as e:
			import traceback
			traceback.print_exc()
			print('Exception: {}'.format(e))
			sleep(5)
			continue

		y_rpn_reg *= stddev_scaling

		idx += 1

		yield np.expand_dims(img, axis=0), [y_rpn_cls, y_rpn_reg], params