import keras
from keras import layers
from keras.models import Sequential, model_from_json, Model
from keras.layers import Activation, Dense, Dropout, Flatten, Input
from keras.layers import Conv2D, MaxPooling2D, LocallyConnected2D
from keras.layers.normalization import BatchNormalization
from keras.optimizers import Adam, Nadam, RMSprop
from keras import regularizers
from keras.initializers import RandomUniform, RandomNormal
import keras.backend as K
from keras.utils import generic_utils

from  RoiPoolingConv import RoiPoolingConv

import os
os.environ['TF_CPP_MIN_LOG_LEVEL']='2'

import tensorflow as tf
import numpy as np
import json
import matplotlib.pyplot as plt
from itertools import cycle
from time import sleep
from time import strftime

from keras import backend as K
from keras.layers.advanced_activations import LeakyReLU, PReLU

import traceback
import h5py
import sys
sys.path.insert(0, 'pt_cloud')
from pt_cloud_consts import *

import models
import data_loader
import rpn_utils

class truncfloat(float):
    def __repr__(self):
        return "%0.2f" % self

# running from command line
# ~$ python train_keras_cnn.py [train_dir [model_dir]] [loadmod] [savemod] [rpnbatchX] [regbatchX] [epochX]

# train_dir : where to find training data; defaults to data/train/
# test_dir : where to find testing (evaluation) data; if ommited, it's set to train_dir
# model_dir : where to find the saved model; if ommited, it's set to train_dir

# the rest are switches, that make something happen when they are included
# loadmod : attempt to load model from model_dir; if it fails, it builds a new model
# savemod : save model to model_dir at end of execution
# rpnbatchX : how big rpn batch sizes should be (num img per iteration); X should be replaced with an integer (defaults to 1, not implemented)
# regbatchX : how big reg batch sizes should be (num boxes per train op); X should be replaced with an integer (defaults to 6)
# epochX : how many epochs to train on; X should be replaced with an integer (defaults to 2)

# switches can be anywhere/any order, but it's generally a good idea to put them at the end 
# the order of the other (first) 2 params is important though

# ------ EDITABLE PARAMS ------

anchor_scales = [64, 128, 256]
anchor_shapes = [[1, 1], [1, 2], [2, 1]]

stddev_scaling = 4.0

# size of feature maps output from rpn
out_dim = (36,36)

# ------ /EDITABLE PARAMS ------

num_params = 6
img_rows = BINS_H
img_cols = BINS_W

# 1 epoch = 1 traversall of all data
# num_data/batch_size = num (train) iterations in single epoch
# reg_batch_sz = number of rois to predict params on per iteration
# rpn_batch_sz = number of imgs to predict boxes on per iteration (not implemented)
rpn_batch_sz = 1
reg_batch_sz = 6
epochs = 2
# num iterations gets set after data gets loaded

load_model_from_file = False
save_model_to_file = False

train_param_csv = "data/train/scenes.h5"
test_param_csv = "data/test/scenes.h5"

train_bbox_csv = "data/train/scenes_bboxes.csv"
test_bbox_csv = "data/test/scenes_bboxes.csv"


model_dir = "data/model"

#  ----------------- Load cmdline params -----------------

args = sys.argv[1:]

try:
	for p in args:
		if p.startswith("rpnbatch"):
			args.remove(p)
			n = p[8:]
			if len(n) > 0:
				rpn_batch_sz = int(n)
except:
	pass
try:
	for p in args:
		if p.startswith("regbatch"):
			args.remove(p)
			n = p[8:]
			if len(n) > 0:
				reg_batch_sz = int(n)
except:
	pass
try:
	for p in args:
		if p.startswith("epoch"):
			args.remove(p)
			n = p[5:]
			if len(n) > 0:
				epochs = int(n)
except:
	pass
try:
	args.remove("loadmod")
	load_model_from_file = True
except:
	load_model_from_file = False
try:
	args.remove("savemod")
	save_model_to_file = True
except:
	save_model_to_file = False

if len(args) > 0:
	train_param_csv = args[0]+"/scenes.h5"
	train_bbox_csv = args[0]+"/scenes_bboxes.csv"
else:
	print "Must specify data dir"
	exit(1)

if len(args) > 1:
	model_dir = args[1]
else:
	model_dir = train_param_csv[:-10]

#  -----------------  -----------------  -----------------

def split_array(a, els=6):
	return [a[i:i+els] for i in xrange(0, len(a), els)]

def split_array_expanded(a, els=6):
	return [np.expand_dims(a[i:i+els],0) for i in xrange(0, len(a), els)]

assert_dir(model_dir)

x_train = data_loader.load_h5_imgparam_dataset(train_param_csv, img_rows, img_cols)
bbox_train = data_loader.load_bboxes_labels(train_bbox_csv)

input_shape = (img_rows, img_cols, 1)

print 'train data shape:', x_train.shape
print 'roi train shape:', bbox_train.shape

iterations_per_epoch = x_train.shape[0]/rpn_batch_sz

# zip 'em together to get [ ( img[img_h[float...]], [(bbox, params)...] )... ]
train = zip(x_train, bbox_train)

datagen = data_loader.data_generator(train, anchor_scales, anchor_shapes, stddev_scaling=stddev_scaling, out_dim=out_dim)

rpn_model = Sequential()
reg_model = Sequential()
model = Sequential()
if load_model_from_file and os.path.isfile(model_dir+"/model.json") and os.path.isfile(model_dir+"/rpn_model.json") and os.path.isfile(model_dir+"/reg_model.json"):

	json_file = open(model_dir+"/rpn_model.json", "r")
	loaded_model_json = json_file.read()
	json_file.close()
	rpn_model = model_from_json(loaded_model_json)
	rpn_model.load_weights(model_dir+"/rpn_model.h5")
	
	json_file = open(model_dir+"/reg_model.json", "r")
	loaded_model_json = json_file.read()
	json_file.close()
	reg_model = model_from_json(loaded_model_json, custom_objects={'RoiPoolingConv': RoiPoolingConv})
	reg_model.load_weights(model_dir+"/reg_model.h5")
	

	json_file = open(model_dir+"/model.json", "r")
	loaded_model_json = json_file.read()
	json_file.close()
	model = model_from_json(loaded_model_json, custom_objects={'RoiPoolingConv': RoiPoolingConv})
	model.load_weights(model_dir+"/model.h5")

	print "Loaded model from",model_dir

else:
	img_input = Input(shape=input_shape)
	roi_input = Input(shape=(reg_batch_sz,4,))

	shared_layers = models.squeezenet(img_input)
	x_cls,x_reg = models.rpn(shared_layers, len(anchor_scales)*len(anchor_shapes))

	out_params = models.regressor(shared_layers, roi_input, num_params, reg_batch_sz)

	rpn_model = Model(img_input, [x_cls,x_reg])
	reg_model = Model([img_input, roi_input], out_params)

	model = Model([img_input, roi_input], [x_cls, x_reg, out_params])


opt = Adam(lr=0.001, beta_1=0.9, beta_2=0.999, epsilon=1e-08, decay=0.0)
rpn_model.compile(loss='mean_squared_error', optimizer=opt, metrics=['mean_squared_error'])
reg_model.compile(loss='mean_squared_error', optimizer=opt, metrics=['mean_squared_error'])
model.compile(optimizer='sgd', loss='mae')


# [loss cls_rpn, loss reg_rpn, loss regr, acc boxes, acc params]
loss_acc = np.full((epochs*iterations_per_epoch, 5),np.nan)

for epoch_n in xrange(epochs):

	progbar = generic_utils.Progbar(iterations_per_epoch)
	print('Epoch {}/{}'.format(epoch_n + 1, epochs))

	for iter_n in xrange(iterations_per_epoch):
		try:
			X_img,Y_rpn,params = datagen.next()
			# X_img ~ img
			# Y_rpn ~ [rpn_cls, rpn_reg] labels
			# params ~ [([x,y,x,y], [A,B,C,r,s,t])...]

			loss_rpn = rpn_model.train_on_batch(X_img, Y_rpn)
			pred_rpn = rpn_model.predict_on_batch(X_img)

			# convert rpn feature maps to rois; select only boxes with highest objectness score
			boxes = rpn_utils.rpn_to_roi(pred_rpn[0], pred_rpn[1], anchor_scales, anchor_shapes, X_img.shape[1:3], stddev_scaling=stddev_scaling, out_dim=out_dim, max_boxes=300)
			# boxes ~ [[x,y,x,y], ...]

			X_boxes_gt, Y_params_gt = rpn_utils.calc_parametrization_labels_independant(params, X_img.shape[1:3], out_dim=out_dim)
			X_boxes, Y_params, _ = rpn_utils.calc_parametrization_labels_nolimits(boxes, params, X_img.shape[1:3], out_dim=out_dim)
			# X_boxes ~ [[x,y,w,h], ...]
			# Y_params ~ [[A,B,C,r,s,t], ...]
			# Y_error ~ [[x,y,w,h], ...] rpn error in relation to gt

			if X_boxes is None:
				print "ROIs found on this image are None"
				continue

			# print "gt boxes prescale (xywh) - gt boxes (xywh) - gt params:"
			# for i in range(len(X_boxes_gt[0])):
			# 	argh = [(params[i][0][0]+params[i][0][2])/2.,(params[i][0][1]+params[i][0][3])/2.,params[i][0][2]-params[i][0][0],params[i][0][3]-params[i][0][1]]
			# 	print map(truncfloat,argh),map(truncfloat,X_boxes_gt[0][i]),map(truncfloat,Y_params_gt[0][i])

			# print "Num boxes predicted:",len(X_boxes[0])

			box_acc = 0.0
			par_acc = 0.0

			# calc box placement accuracy (distance between gt & predicted vectors)
			# foreach predicted box
			for i in xrange(len(X_boxes[0])):
				# check all gt boxes
				for j in range(len(X_boxes_gt[0])):
					# if params for gt & predicted box are basically the same (both params are gt, isclose just because floats)
					if np.isclose(Y_params[0,i,:],Y_params_gt[0][j]).all(): #.any():
						# calc distance
						box_dist = np.linalg.norm(X_boxes_gt[0][j]-X_boxes[:,i,:])
						box_acc += box_dist
						# print "gt params:",map(truncfloat,Y_params_gt[0][j])
						# print "predict xbox:",X_boxes[:,i,:],"\npredict xbox upscale:",map(truncfloat,(X_boxes[:,i,:]/0.06)[0]),"\n"
						# process next predicted box
						break

			# normalize accuracy
			box_acc = box_acc/len(X_boxes[0])


			# split predicted boxes into batches
			X_boxes_batches = split_array_expanded(X_boxes[0], reg_batch_sz)
			Y_params_batches = split_array_expanded(Y_params[0], reg_batch_sz)
			# discard last batch if it contains less than reg_batch_sz boxes, all other batches guaranteed to have that many
			if len(X_boxes_batches[-1][0]) != reg_batch_sz:
				X_boxes_batches.pop()
				Y_params_batches.pop()

			if len(X_boxes_batches) < 1:
				print "Not enough ROIs found in this image"
				continue

			# iterate over all batches
			for i in xrange(len(X_boxes_batches)):
				# train regressor on batch
				loss_reg = reg_model.train_on_batch([X_img, X_boxes_batches[i]], Y_params_batches[i])
				# get predictions for batch
				pred_param = reg_model.predict_on_batch([X_img, X_boxes_batches[i]])

				# for j in xrange(reg_batch_sz):
				# 	print "gt params:",map(truncfloat,Y_params_batches[i][0][j])
				# 	print "pred params:",map(truncfloat,pred_param[0][j]),"\n"

				# calc accuracy from predictions
				par_dist = np.linalg.norm(Y_params_batches[i]-pred_param)
				par_acc += par_dist

			# normalize accuracy
			par_acc = par_acc/(reg_batch_sz*len(X_boxes_batches))

			curr_iter = epoch_n*iterations_per_epoch + iter_n

			loss_acc[curr_iter, 0] = loss_rpn[1]
			loss_acc[curr_iter, 1] = loss_rpn[2]
			loss_acc[curr_iter, 2] = loss_reg[0]
			loss_acc[curr_iter, 3] = box_acc
			loss_acc[curr_iter, 4] = par_acc

			progbar.update(
				(curr_iter)%(iterations_per_epoch) + 1, 
				[
					('mean_rpn_cls_loss', np.nanmean(loss_acc[:curr_iter+1, 0])), 
					('mean_rpn_reg_loss', np.nanmean(loss_acc[:curr_iter+1, 1])),
					('mean_par_reg_loss', np.nanmean(loss_acc[:curr_iter+1, 2])),
					('mean_rpn_acc', np.nanmean(loss_acc[:curr_iter+1, 3])),
					('mean_par_acc', np.nanmean(loss_acc[:curr_iter+1, 4]))
				]
			)
			print "\n"

		except Exception as e:
			traceback.print_exc()
			print('Exception: {}'.format(e))
			continue


with h5py.File(model_dir+"/loss_acc"+strftime("%y%m%d_%H%M%S")+".h5", 'w') as hf:
	hf.create_dataset("loss_acc",  data=loss_acc)

if save_model_to_file:
	if os.path.isfile(model_dir+"/model.json") or os.path.isfile(model_dir+"/rpn_model.json") or os.path.isfile(model_dir+"/reg_model.json"):
		try:
			os.rename(model_dir+"/model.json", model_dir+"/model.json.bak")
			os.rename(model_dir+"/model.h5", model_dir+"/model.h5.bak")
		except:
			pass
		try:
			os.rename(model_dir+"/rpn_model.json", model_dir+"/rpn_model.json.bak")
			os.rename(model_dir+"/rpn_model.h5", model_dir+"/rpn_model.h5.bak")
		except:
			pass
		try:
			os.rename(model_dir+"/reg_model.json", model_dir+"/reg_model.json.bak")
			os.rename(model_dir+"/reg_model.h5", model_dir+"/reg_model.h5.bak")
		except:
			pass

	model_json = model.to_json()
	with open(model_dir+"/model.json", "w") as json_file:
		json_file.write(model_json)
	model.save_weights(model_dir+"/model.h5")

	model_json = rpn_model.to_json()
	with open(model_dir+"/rpn_model.json", "w") as json_file:
		json_file.write(model_json)
	rpn_model.save_weights(model_dir+"/rpn_model.h5")

	model_json = reg_model.to_json()
	with open(model_dir+"/reg_model.json", "w") as json_file:
		json_file.write(model_json)
	reg_model.save_weights(model_dir+"/reg_model.h5")

	print"Saved model to",model_dir

# from quiver_engine import server
# assert_dir("tmp/")
# server.launch(model, input_folder=train_param_csv[:train_param_csv.rfind("/")+1])