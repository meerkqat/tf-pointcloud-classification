import numpy as np
import data_loader
import copy

def apply_regr_np(X, T):
	try:
		x = X[0, :, :]
		y = X[1, :, :]
		w = X[2, :, :]
		h = X[3, :, :]

		tx = T[0, :, :]
		ty = T[1, :, :]
		tw = T[2, :, :]
		th = T[3, :, :]

		cx = x + w/2.
		cy = y + h/2.
		cx1 = tx * w + cx
		cy1 = ty * h + cy

		w1 = np.exp(tw) * w
		h1 = np.exp(th) * h
		x1 = cx1 - w1/2.
		y1 = cy1 - h1/2.

		x1 = np.round(x1)
		y1 = np.round(y1)
		w1 = np.round(w1)
		h1 = np.round(h1)
		return np.stack([x1, y1, w1, h1])
	except Exception as e:
		print(e)
		return X

def non_max_suppression_fast(boxes, probs, overlap_thresh=0.9, max_boxes=300):
	# code used from here: http://www.pyimagesearch.com/2015/02/16/faster-non-maximum-suppression-python/
	# if there are no boxes, return an empty list
	if len(boxes) == 0:
		return []

	# grab the coordinates of the bounding boxes
	x1 = boxes[:, 0]
	y1 = boxes[:, 1]
	x2 = boxes[:, 2]
	y2 = boxes[:, 3]

	np.testing.assert_array_less(x1, x2)
	np.testing.assert_array_less(y1, y2)

	# if the bounding boxes integers, convert them to floats --
	# this is important since we'll be doing a bunch of divisions
	if boxes.dtype.kind == "i":
		boxes = boxes.astype("float")

	# initialize the list of picked indexes	
	pick = []

	# calculate the areas
	area = (x2 - x1) * (y2 - y1)

	# sort the bounding boxes 
	idxs = np.argsort(probs)

	# keep looping while some indexes still remain in the indexes
	# list
	while len(idxs) > 0:
		# grab the last index in the indexes list and add the
		# index value to the list of picked indexes
		last = len(idxs) - 1
		i = idxs[last]
		pick.append(i)

		# find the intersection

		xx1_int = np.maximum(x1[i], x1[idxs[:last]])
		yy1_int = np.maximum(y1[i], y1[idxs[:last]])
		xx2_int = np.minimum(x2[i], x2[idxs[:last]])
		yy2_int = np.minimum(y2[i], y2[idxs[:last]])

		ww_int = np.maximum(0, xx2_int - xx1_int)
		hh_int = np.maximum(0, yy2_int - yy1_int)

		area_int = ww_int * hh_int

		# find the union
		area_union = area[i] + area[idxs[:last]] - area_int

		# compute the ratio of overlap
		overlap = area_int/(area_union + 1e-6)

		# delete all indexes from the index list that have
		idxs = np.delete(idxs, np.concatenate(([last],
			np.where(overlap > overlap_thresh)[0])))

		if len(pick) >= max_boxes:
			break

	# return only the bounding boxes that were picked using the integer data type
	boxes = boxes[pick].astype("int")
	probs = probs[pick]
	return boxes, probs

def rpn_to_roi(clsf_layer, regr_layer, anchor_scales, anchor_shapes, img_res, max_boxes=300, overlap_thresh=0.9, stddev_scaling=0.4, out_dim=(13,13)):
	# size of feature maps output from rpn
	out_w,out_h = out_dim

	regr_layer = regr_layer / stddev_scaling

	assert clsf_layer.shape[0] == 1

	(rows, cols) = clsf_layer.shape[1:3]

	scale_x = (float(out_w)/img_res[1])
	scale_y = (float(out_h)/img_res[0])

	curr_layer = 0

	# A.shape = (4boxparams, rows, cols, n_bbox_types)
	A = np.zeros((4, clsf_layer.shape[1], clsf_layer.shape[2], clsf_layer.shape[3]))
	
	for anchor_scale in anchor_scales:
		for anchor_shape in anchor_shapes:
			anchor_w = (anchor_scale * anchor_shape[0])*scale_x
			anchor_h = (anchor_scale * anchor_shape[1])*scale_y
			
			# get next regression slice [13x13x4]
			regr = regr_layer[0, :, :, 4 * curr_layer:4 * curr_layer + 4]
			regr = np.transpose(regr, (2, 0, 1))

			# cartesian product of X & Y would produce all integer coordinates from (0,0) to (cols,rows)
			X, Y = np.meshgrid(np.arange(cols),np. arange(rows))

			# set up base param estimation x,y,w,h
			A[0, :, :, curr_layer] = X - anchor_w/2
			A[1, :, :, curr_layer] = Y - anchor_h/2
			A[2, :, :, curr_layer] = anchor_w
			A[3, :, :, curr_layer] = anchor_h

			# adjust base estimation with obtained regression results
			A[:, :, :, curr_layer] = apply_regr_np(A[:, :, :, curr_layer], regr)

			# clamp coordinates to image edges if they go over and convert from xywh -> xyxy
			A[2, :, :, curr_layer] = np.maximum(1, A[2, :, :, curr_layer])
			A[3, :, :, curr_layer] = np.maximum(1, A[3, :, :, curr_layer])
			A[2, :, :, curr_layer] += A[0, :, :, curr_layer]
			A[3, :, :, curr_layer] += A[1, :, :, curr_layer]

			A[0, :, :, curr_layer] = np.maximum(0, A[0, :, :, curr_layer])
			A[1, :, :, curr_layer] = np.maximum(0, A[1, :, :, curr_layer])
			A[2, :, :, curr_layer] = np.minimum(cols-1, A[2, :, :, curr_layer])
			A[3, :, :, curr_layer] = np.minimum(rows-1, A[3, :, :, curr_layer])

			curr_layer += 1

	all_boxes = np.reshape(A.transpose((0, 3, 1,2)), (4, -1)).transpose((1, 0))
	all_probs = clsf_layer.transpose((0, 3, 1, 2)).reshape((-1))

	# dump boxes where coordinate magnitudes are swapped i.e. x2 > x1, y2 > y1
	x1 = all_boxes[:, 0]
	y1 = all_boxes[:, 1]
	x2 = all_boxes[:, 2]
	y2 = all_boxes[:, 3]

	idxs = np.where((x1 - x2 >= 0) | (y1 - y2 >= 0))

	all_boxes = np.delete(all_boxes, idxs, 0)
	all_probs = np.delete(all_probs, idxs, 0)
	# all boxes ~ [[x,y,x,y], ...]
	# all probs ~ [prob, ...]

	# select boxes by best objectness score (clsf_layer) and they don't overlap too much
	# also probably squeeze boxes to fit tightly around object
	nmsboxes,nmsprobs = non_max_suppression_fast(all_boxes, all_probs, overlap_thresh=overlap_thresh, max_boxes=max_boxes)

	return nmsboxes

def calc_parametrization_labels_independant(params, img_res, out_dim=(13,13)):
	# size of feature maps output from rpn
	out_w,out_h = out_dim

	bboxes = np.array(params)[:,0]
	param_labels = np.array(params)[:,1]

	scale_x = (float(out_w)/img_res[1])
	scale_y = (float(out_h)/img_res[0])

	# init outputs
	X = np.zeros((len(bboxes),4))
	Y = np.zeros((len(param_labels),6))

	# scale ground truth boxes down to rpn output size
	for i in xrange(len(bboxes)):
		x1 = bboxes[i][0] * scale_x
		y1 = bboxes[i][1] * scale_y
		x2 = bboxes[i][2] * scale_x
		y2 = bboxes[i][3] * scale_y

		X[i][0] = x1
		X[i][1] = y1
		#X[i][0] = (x2+x1)/2.
		#X[i][1] = (y2+y1)/2.
		X[i][2] = (x2-x1)
		X[i][3] = (y2-y1)

	for i in xrange(len(param_labels)):
		for j in xrange(len(param_labels[i])):
			Y[i][j] = param_labels[i][j]

	return np.expand_dims(X, axis=0), np.expand_dims(Y, axis=0)

# params = [(bbox, params)...] i.e. [ ([x,y,x,y],[A,B,C,r,s,t])... ]
# img_res = (input_img_height, input_img_width)
def calc_parametrization_labels(rpn_boxes, params, img_res, out_dim=(13,13)):
	# size of feature maps output from rpn
	out_w,out_h = out_dim

	regr_stddev = [8.0, 8.0, 4.0, 4.0]

	MAX_OVERLAP = 0.7
	MIN_OVERLAP = 0.3

	# don't overwrite params
	bboxes = np.array(copy.deepcopy(params))[:,0]

	n_bboxes = len(bboxes)
	scale_x = (float(out_w)/img_res[1])
	scale_y = (float(out_h)/img_res[0])

	# scale ground truth boxes down to rpn output size
	for i in xrange(n_bboxes):
		bboxes[i][0] *= scale_x
		bboxes[i][1] *= scale_y
		bboxes[i][2] *= scale_x
		bboxes[i][3] *= scale_y

	x_roi = []
	y_regr_params = []
	y_regr_errs = []

	for (x1, y1, x2, y2) in rpn_boxes:
		x1 = int(round(x1))
		y1 = int(round(y1))
		x2 = int(round(x2))
		y2 = int(round(y2))

		best_iou = 0.0
		best_bbox = -1
		# find bbox from ground truths that has best iou with current rpn box
		for bbox_idx in range(len(bboxes)):
			curr_iou = data_loader.calc_iou(bboxes[bbox_idx], [x1, y1, x2, y2])
			if curr_iou > best_iou:
				best_iou = curr_iou
				best_bbox = bbox_idx

		# dump boxes with insufficient overlap
		if best_iou < MIN_OVERLAP:
				continue
		else:
			w = x2 - x1
			h = y2 - y1
			#x_roi.append([(x1+x2)/2., (y1+y2)/2., w, h])
			x_roi.append([x1, y1, w, h])

			if MAX_OVERLAP <= best_iou:

				cxgt = (bboxes[best_bbox][0] + bboxes[best_bbox][2]) / 2.
				cygt = (bboxes[best_bbox][1] + bboxes[best_bbox][3]) / 2.

				cx = x1 + w / 2.
				cy = y1 + h / 2.

				tx = (cxgt - cx) / float(w)
				ty = (cygt - cy) / float(h)
				tw = np.log((bboxes[best_bbox][2] - bboxes[best_bbox][0]) / float(w))
				th = np.log((bboxes[best_bbox][3] - bboxes[best_bbox][1]) / float(h))

				y_regr_params.append(params[best_bbox][1])
				y_regr_errs.append([regr_stddev[0]*tx, regr_stddev[1]*ty, regr_stddev[2]*tw, regr_stddev[3]*th])

	if len(x_roi) == 0:
		return None, None, None

	X = np.array(x_roi)
	Y1 = np.array(y_regr_params)
	Y2 = np.array(y_regr_errs)

	return np.expand_dims(X, axis=0), np.expand_dims(Y1, axis=0), np.expand_dims(Y2, axis=0)

# params = [(bbox, params)...] i.e. [ ([x,y,x,y],[A,B,C,r,s,t])... ]
# img_res = (input_img_height, input_img_width)
def calc_parametrization_labels_nolimits(rpn_boxes, params, img_res, out_dim=(13,13)):
	# size of feature maps output from rpn
	out_w,out_h = out_dim

	regr_stddev = [8.0, 8.0, 4.0, 4.0]

	# don't overwrite params
	bboxes = np.array(copy.deepcopy(params))[:,0]

	n_bboxes = len(bboxes)
	scale_x = (float(out_w)/img_res[1])
	scale_y = (float(out_h)/img_res[0])

	# scale ground truth boxes down to rpn output size
	for i in xrange(n_bboxes):
		bboxes[i][0] *= scale_x
		bboxes[i][1] *= scale_y
		bboxes[i][2] *= scale_x
		bboxes[i][3] *= scale_y

	x_roi = []
	y_regr_params = []
	y_regr_errs = []

	for (x1, y1, x2, y2) in rpn_boxes:
		best_iou = 0.0
		best_bbox = -1
		# find bbox from ground truths that has best iou with current rpn box
		for bbox_idx in range(len(bboxes)):
			curr_iou = data_loader.calc_iou(bboxes[bbox_idx], [x1, y1, x2, y2])
			if curr_iou > best_iou:
				best_iou = curr_iou
				best_bbox = bbox_idx

		if best_iou > 0:
			w = x2 - x1
			h = y2 - y1

			cxgt = (bboxes[best_bbox][0] + bboxes[best_bbox][2]) / 2.
			cygt = (bboxes[best_bbox][1] + bboxes[best_bbox][3]) / 2.

			cx = x1 + w / 2.
			cy = y1 + h / 2.

			tx = (cxgt - cx) / float(w)
			ty = (cygt - cy) / float(h)
			tw = np.log((bboxes[best_bbox][2] - bboxes[best_bbox][0]) / float(w))
			th = np.log((bboxes[best_bbox][3] - bboxes[best_bbox][1]) / float(h))

			#x_roi.append([(x1+x2)/2., (y1+y2)/2., w, h])
			x_roi.append([x1, y1, w, h])
			y_regr_params.append(params[best_bbox][1])
			y_regr_errs.append([regr_stddev[0]*tx, regr_stddev[1]*ty, regr_stddev[2]*tw, regr_stddev[3]*th])

	if len(x_roi) == 0:
		return None, None, None

	X = np.array(x_roi)
	Y1 = np.array(y_regr_params)
	Y2 = np.array(y_regr_errs)

	return np.expand_dims(X, axis=0), np.expand_dims(Y1, axis=0), np.expand_dims(Y2, axis=0)

# gt params really only necessary to be able to simply iterate over all possible params for this image
# could get them from set(params) if nexessary maybe
def aggregate_regions(boxes, params, gtparams):
	box_acc = []
	param_acc = []

	# iterate over all gt params
	for p in gtparams:
		# get indexes of boxes/params for current gt param
		idxs = np.where((np.isclose(params,p)).all(axis=1))[0]

		# make sure we have detections for this object, otherwise we have to skip it
		if len(idxs) < 1:
			continue

		# set first (best) box/param
		param_acc.append(params[idxs[0]])
		box_acc.append([0.,0.,0.,0.])
		
		# aggregate the rest
		for idx in idxs:
			box_acc[-1][0] += boxes[idx][0]
			box_acc[-1][1] += boxes[idx][1]
			box_acc[-1][2] += boxes[idx][2]
			box_acc[-1][3] += boxes[idx][3]

		# normalize aggregation
		box_acc[-1][0] /= float(len(idxs))
		box_acc[-1][1] /= float(len(idxs))
		box_acc[-1][2] /= float(len(idxs))
		box_acc[-1][3] /= float(len(idxs))

	# return formatting
	box_acc = np.array(box_acc)
	param_acc = np.array(param_acc)
	
	return np.expand_dims(box_acc, axis=0), np.expand_dims(param_acc, axis=0)