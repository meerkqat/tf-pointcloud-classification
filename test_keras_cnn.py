import keras
from keras import layers
from keras.models import Sequential, model_from_json, Model
from keras.layers import Activation, Dense, Dropout, Flatten, Input
from keras.layers import Conv2D, MaxPooling2D, LocallyConnected2D
from keras.layers.normalization import BatchNormalization
from keras.optimizers import Adam, Nadam, RMSprop
from keras import regularizers
from keras.initializers import RandomUniform, RandomNormal
import keras.backend as K
from keras.utils import generic_utils

from  RoiPoolingConv import RoiPoolingConv

import os
os.environ['TF_CPP_MIN_LOG_LEVEL']='2'

import tensorflow as tf
import numpy as np
import json
import matplotlib.pyplot as plt
from itertools import cycle
from time import sleep
from time import strftime

from keras import backend as K
from keras.layers.advanced_activations import LeakyReLU, PReLU

import traceback
import h5py
import sys
sys.path.insert(0, 'pt_cloud')
from pt_cloud_consts import *

import models
import data_loader
import rpn_utils

class truncfloat(float):
    def __repr__(self):
        return "%0.2f" % self

# running from command line
# ~$ python test_keras_cnn.py [test_dir [model_dir]] [rpnbatchX] [regbatchX]

# test_dir : where to find testing (evaluation) data; if ommited, it's set to train_dir
# model_dir : where to find the saved model; if ommited, it's set to train_dir

# rpnbatchX : how big rpn batch sizes should be (num img per iteration); X should be replaced with an integer (defaults to 1)
# regbatchX : how big reg batch sizes should be (num boxes per train op); X should be replaced with an integer (defaults to 6)

# switches can be anywhere/any order, but it's generally a good idea to put them at the end 
# the order of the other (first) 2 params is important though

# ------ EDITABLE PARAMS ------

anchor_scales = [64, 128, 256]
anchor_shapes = [[1, 1], [1, 2], [2, 1]]

stddev_scaling = 4.0

# size of feature maps output from rpn
out_dim = (36,36)

# ------ /EDITABLE PARAMS ------

rpn_batch_sz = 1
reg_batch_sz = 6

img_rows = BINS_H
img_cols = BINS_W

test_param_csv = "data/test/scenes.h5"
test_bbox_csv = "data/test/scenes_bboxes.csv"

model_dir = "data/model"

#  ----------------- Load cmdline params -----------------

args = sys.argv[1:]

try:
	for p in args:
		if p.startswith("rpnbatch"):
			args.remove(p)
			n = p[8:]
			if len(n) > 0:
				rpn_batch_sz = int(n)
except:
	pass
try:
	for p in args:
		if p.startswith("regbatch"):
			args.remove(p)
			n = p[8:]
			if len(n) > 0:
				reg_batch_sz = int(n)
except:
	pass

if len(args) > 0:
	test_param_csv = args[0]+"/scenes.h5"
	test_bbox_csv = args[0]+"/scenes_bboxes.csv"
else:
	print "Must specify data dir"
	exit(1)

if len(args) > 1:
	model_dir = args[1]
else:
	model_dir = test_param_csv[:-10]

#  -----------------  -----------------  -----------------

def split_array(a, els=6):
	return [a[i:i+els] for i in xrange(0, len(a), els)]

def split_array_expanded(a, els=6):
	return [np.expand_dims(a[i:i+els],0) for i in xrange(0, len(a), els)]

x_test = data_loader.load_h5_imgparam_dataset(test_param_csv, img_rows, img_cols)
bbox_test = data_loader.load_bboxes_labels(test_bbox_csv)

print 'test data shape:', x_test.shape
print 'roi test shape:', bbox_test.shape,"\n"

num_iterations = x_test.shape[0]

# zip 'em together to get [ ( img[img_h[float...]], [(bbox, params)...] )... ]
test = zip(x_test, bbox_test)

datagen = data_loader.data_generator(test, anchor_scales, anchor_shapes, stddev_scaling=stddev_scaling, out_dim=out_dim)

rpn_model = Sequential()
reg_model = Sequential()
model = Sequential()

json_file = open(model_dir+"/rpn_model.json", "r")
loaded_model_json = json_file.read()
json_file.close()
rpn_model = model_from_json(loaded_model_json)
rpn_model.load_weights(model_dir+"/rpn_model.h5")

json_file = open(model_dir+"/reg_model.json", "r")
loaded_model_json = json_file.read()
json_file.close()
reg_model = model_from_json(loaded_model_json, custom_objects={'RoiPoolingConv': RoiPoolingConv})
reg_model.load_weights(model_dir+"/reg_model.h5")


json_file = open(model_dir+"/model.json", "r")
loaded_model_json = json_file.read()
json_file.close()
model = model_from_json(loaded_model_json, custom_objects={'RoiPoolingConv': RoiPoolingConv})
model.load_weights(model_dir+"/model.h5")

print "Loaded model from",model_dir

opt = Adam(lr=0.001, beta_1=0.9, beta_2=0.999, epsilon=1e-08, decay=0.0)
rpn_model.compile(loss='mean_squared_error', optimizer=opt, metrics=['mean_squared_error'])
reg_model.compile(loss='mean_squared_error', optimizer=opt, metrics=['mean_squared_error'])
model.compile(optimizer='sgd', loss='mae')

# [acc boxes, acc params]
batch_acc = np.full((num_iterations, 2),np.nan)

progbar = generic_utils.Progbar(num_iterations)

for curr_iter in xrange(num_iterations):
	try:
		X_img,_,params = datagen.next()
		# X_img ~ img
		# params ~ [([x,y,x,y], [A,B,C,r,s,t])...]

		pred_rpn = rpn_model.predict_on_batch(X_img)

		boxes = rpn_utils.rpn_to_roi(pred_rpn[0], pred_rpn[1], anchor_scales, anchor_shapes, X_img.shape[1:3], stddev_scaling=stddev_scaling, out_dim=out_dim, max_boxes=300)
		# boxes ~ [[x,y,x,y], ...]

		X_boxes_gt, Y_params_gt = rpn_utils.calc_parametrization_labels_independant(params, X_img.shape[1:3], out_dim=out_dim)
		X_boxes, Y_params, _ = rpn_utils.calc_parametrization_labels_nolimits(boxes, params, X_img.shape[1:3], out_dim=out_dim)
		# X_boxes ~ [[x,y,w,h], ...]
		# Y_params ~ [[A,B,C,r,s,t], ...]

		if X_boxes is None:
			print "ROIs found on this image are None"
			continue

		# print "gt boxes prescale (xywh) - gt boxes (xywh) - gt params:"
		# for i in range(len(X_boxes_gt[0])):
		# 	argh = [(params[i][0][0]+params[i][0][2])/2.,(params[i][0][1]+params[i][0][3])/2.,params[i][0][2]-params[i][0][0],params[i][0][3]-params[i][0][1]]
		# 	print map(truncfloat,argh),map(truncfloat,X_boxes_gt[0][i]),map(truncfloat,Y_params_gt[0][i])

		box_acc = 0.0
		par_acc = 0.0

		# calc box placement accuracy (distance between gt & predicted vectors)
		# foreach predicted box
		for i in xrange(len(X_boxes[0])):
			# check all gt boxes
			for j in range(len(X_boxes_gt[0])):
				# if params for gt & predicted box are basically the same (both params are gt, isclose just because floats)
				if np.isclose(Y_params[0,i,:],Y_params_gt[0][j]).all():
					# calc distance
					box_dist = np.linalg.norm(X_boxes_gt[0][j]-X_boxes[:,i,:])
					box_acc += box_dist
					# print "gt params:",map(truncfloat,Y_params_gt[0][j])
					# print "predict xbox:",X_boxes[:,i,:],"\npredict xbox upscale:",map(truncfloat,(X_boxes[:,i,:]/0.06)[0]),"\n"					
					# process next predicted box
					break

		# normalize accuracy
		box_acc = box_acc/len(X_boxes[0])

		# print "------------------------------------------------------------"

		# split predicted boxes into batches
		X_boxes_batches = split_array_expanded(X_boxes[0], reg_batch_sz)
		Y_params_batches = split_array_expanded(Y_params[0], reg_batch_sz)
		# discard last batch if it contains less than reg_batch_sz boxes, all other batches guaranteed to have that many
		if len(X_boxes_batches[-1][0]) != reg_batch_sz:
			X_boxes_batches.pop()
			Y_params_batches.pop()

		if len(X_boxes_batches) < 1:
			print "Not enough ROIs found in this image"
			continue

		# iterate over all batches
		for i in xrange(len(X_boxes_batches)):
			# get predictions for batch
			pred_param = reg_model.predict_on_batch([X_img, X_boxes_batches[i]])
			# param shape == (1,num_roi,6)

			# for j in xrange(reg_batch_sz):
			# 	print "gt params:",map(truncfloat,Y_params_batches[i][0][j])
			# 	print "pred params:",map(truncfloat,pred_param[0][j]),"\n"

			# calc accuracy from predictions
			par_dist = np.linalg.norm(Y_params_batches[i]-pred_param)
			par_acc += par_dist

		# normalize accuracy
		par_acc = par_acc/(reg_batch_sz*len(X_boxes_batches))


		batch_acc[curr_iter, 0] = box_acc
		batch_acc[curr_iter, 1] = par_acc

		progbar.update(
			(curr_iter)+1, 
			[
				('mean_rpn_acc', np.nanmean(batch_acc[:curr_iter+1, 0])),
				('mean_par_acc', np.nanmean(batch_acc[:curr_iter+1, 1]))
			]
		)
		print "\n"


	except Exception as e:
		traceback.print_exc()
		print('Exception: {}'.format(e))
		continue

with h5py.File(model_dir+"/batch_acc"+strftime("%y%m%d_%H%M%S")+".h5", 'w') as hf:
	hf.create_dataset("batch_acc",  data=batch_acc)