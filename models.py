from keras import layers
from keras.layers import Input, Conv2D, SeparableConv2D, MaxPooling2D, Flatten, Dense, Dropout, TimeDistributed
from keras.layers import ZeroPadding2D, BatchNormalization, Activation, AveragePooling2D, GlobalAveragePooling2D
from  RoiPoolingConv import RoiPoolingConv

# -------------------------- VGG16 --------------------------

def vgg16(input_node, n_outputs=0):
	
	x = Conv2D(64, (3, 3), activation='relu', padding='same')(input_node)
	x = Conv2D(64, (3, 3), activation='relu', padding='same')(x)
	x = MaxPooling2D((2, 2), strides=(2, 2))(x)

	x = Conv2D(128, (3, 3), activation='relu', padding='same')(x)
	x = Conv2D(128, (3, 3), activation='relu', padding='same')(x)
	x = MaxPooling2D((2, 2), strides=(2, 2))(x)

	x = Conv2D(256, (3, 3), activation='relu', padding='same')(x)
	x = Conv2D(256, (3, 3), activation='relu', padding='same')(x)
	x = Conv2D(256, (3, 3), activation='relu', padding='same')(x)
	x = MaxPooling2D((2, 2), strides=(2, 2))(x)

	x = Conv2D(512, (3, 3), activation='relu', padding='same')(x)
	x = Conv2D(512, (3, 3), activation='relu', padding='same')(x)
	x = Conv2D(512, (3, 3), activation='relu', padding='same')(x)
	x = MaxPooling2D((2, 2), strides=(2, 2))(x)

	x = Conv2D(512, (3, 3), activation='relu', padding='same')(x)
	x = Conv2D(512, (3, 3), activation='relu', padding='same')(x)
	x = Conv2D(512, (3, 3), activation='relu', padding='same')(x)
	x = MaxPooling2D((2, 2), strides=(2, 2))(x)

	if n_outputs > 0:
		# x = Flatten()(x)
		# x = Dense(4096, activation='relu')(x)
		# x = Dense(4096, activation='relu')(x)
		# x = Dense(n_outputs, activation='softmax')(x)

		x = Flatten()(x)
		x = Dense(4096, activation='relu')(x)
		x = Dense(4096, activation='relu')(x)
		x = Dense(n_outputs, activation='linear')(x)

	return x

# -------------------------- VGG19 --------------------------

def vgg19(input_node, n_outputs=0):

	x = Conv2D(64, (3, 3), activation='relu', padding='same')(input_node)
	x = Conv2D(64, (3, 3), activation='relu', padding='same')(x)
	x = MaxPooling2D((2, 2), strides=(2, 2))(x)

	x = Conv2D(128, (3, 3), activation='relu', padding='same')(x)
	x = Conv2D(128, (3, 3), activation='relu', padding='same')(x)
	x = MaxPooling2D((2, 2), strides=(2, 2))(x)

	x = Conv2D(256, (3, 3), activation='relu', padding='same')(x)
	x = Conv2D(256, (3, 3), activation='relu', padding='same')(x)
	x = Conv2D(256, (3, 3), activation='relu', padding='same')(x)
	x = Conv2D(256, (3, 3), activation='relu', padding='same')(x)
	x = MaxPooling2D((2, 2), strides=(2, 2))(x)

	x = Conv2D(512, (3, 3), activation='relu', padding='same')(x)
	x = Conv2D(512, (3, 3), activation='relu', padding='same')(x)
	x = Conv2D(512, (3, 3), activation='relu', padding='same')(x)
	x = Conv2D(512, (3, 3), activation='relu', padding='same')(x)
	x = MaxPooling2D((2, 2), strides=(2, 2))(x)

	x = Conv2D(512, (3, 3), activation='relu', padding='same')(x)
	x = Conv2D(512, (3, 3), activation='relu', padding='same')(x)
	x = Conv2D(512, (3, 3), activation='relu', padding='same')(x)
	x = Conv2D(512, (3, 3), activation='relu', padding='same')(x)
	x = MaxPooling2D((2, 2), strides=(2, 2))(x)

	if n_outputs > 0:
		# x = Flatten()(x)
		# x = Dense(4096, activation='relu')(x)
		# x = Dense(4096, activation='relu')(x)
		# x = Dense(n_outputs, activation='softmax')(x)

		x = Flatten()(x)
		x = Dense(4096, activation='relu')(x)
		x = Dense(4096, activation='relu')(x)
		x = Dense(n_outputs, activation='linear')(x)

	return x

# -------------------------- RESNET50 --------------------------

def _resnet50_conv(input_node, kernel_size, filters, stride=(2, 2)):
	x = Conv2D(filters[0], (1, 1), strides=stride)(input_node)
	x = BatchNormalization()(x)
	x = Activation('relu')(x)

	x = Conv2D(filters[1], kernel_size, padding='same')(x)
	x = BatchNormalization()(x)
	x = Activation('relu')(x)

	x = Conv2D(filters[2], (1, 1))(x)
	x = BatchNormalization()(x)

	bypass = Conv2D(filters[2], (1, 1), strides=stride)(input_node)
	bypass = BatchNormalization()(bypass)

	x = layers.add([x, bypass])
	x = Activation('relu')(x)

	return x

def _resnet50_identity(input_node, kernel_size, filters):
	x = Conv2D(filters[0], (1, 1))(input_node)
	x = BatchNormalization()(x)
	x = Activation('relu')(x)

	x = Conv2D(filters[1], kernel_size, padding='same')(x)
	x = BatchNormalization()(x)
	x = Activation('relu')(x)

	x = Conv2D(filters[2], (1, 1))(x)
	x = BatchNormalization()(x)

	x = layers.add([x, input_node])
	x = Activation('relu')(x)

	return x

def resnet50(input_node, n_outputs=0):

	x = ZeroPadding2D((3, 3))(input_node)
	x = Conv2D(64, (7, 7), strides=(2, 2))(x)
	x = BatchNormalization()(x)
	x = Activation('relu')(x)
	x = MaxPooling2D((3, 3), strides=(2, 2))(x)

	x = _resnet50_conv(x, 3, [64, 64, 256], (1, 1))
	x = _resnet50_identity(x, 3, [64, 64, 256])
	x = _resnet50_identity(x, 3, [64, 64, 256])

	x = _resnet50_conv(x, 3, [128, 128, 512])
	x = _resnet50_identity(x, 3, [128, 128, 512])
	x = _resnet50_identity(x, 3, [128, 128, 512])
	x = _resnet50_identity(x, 3, [128, 128, 512])

	x = _resnet50_conv(x, 3, [256, 256, 1024])
	x = _resnet50_identity(x, 3, [256, 256, 1024])
	x = _resnet50_identity(x, 3, [256, 256, 1024])
	x = _resnet50_identity(x, 3, [256, 256, 1024])
	x = _resnet50_identity(x, 3, [256, 256, 1024])
	x = _resnet50_identity(x, 3, [256, 256, 1024])

	x = _resnet50_conv(x, 3, [512, 512, 2048])
	x = _resnet50_identity(x, 3, [512, 512, 2048])
	x = _resnet50_identity(x, 3, [512, 512, 2048])

	x = AveragePooling2D((7, 7))(x)

	if n_outputs > 0:
		# x = Flatten()(x)
		# x = Dense(n_outputs, activation='softmax')(x)

		x = Flatten()(x)
		x = Dense(n_outputs, activation='linear')(x)

	return x

# -------------------------- XCEPTION --------------------------

def xception(input_node, n_outputs=0):

	x = Conv2D(32, (3, 3), strides=(2, 2), use_bias=False)(input_node)
	x = BatchNormalization()(x)
	x = Activation('relu')(x)
	x = Conv2D(64, (3, 3), use_bias=False)(x)
	x = BatchNormalization()(x)
	x = Activation('relu')(x)

	r = Conv2D(128, (1, 1), strides=(2, 2), padding='same', use_bias=False)(x)
	r = BatchNormalization()(r)

	x = SeparableConv2D(128, (3, 3), padding='same', use_bias=False)(x)
	x = BatchNormalization()(x)
	x = Activation('relu')(x)
	x = SeparableConv2D(128, (3, 3), padding='same', use_bias=False)(x)
	x = BatchNormalization()(x)

	x = MaxPooling2D((3, 3), strides=(2, 2), padding='same')(x)
	x = layers.add([x, r])

	r = Conv2D(256, (1, 1), strides=(2, 2), padding='same', use_bias=False)(x)
	r = BatchNormalization()(r)

	x = Activation('relu')(x)
	x = SeparableConv2D(256, (3, 3), padding='same', use_bias=False)(x)
	x = BatchNormalization()(x)
	x = Activation('relu')(x)
	x = SeparableConv2D(256, (3, 3), padding='same', use_bias=False)(x)
	x = BatchNormalization()(x)

	x = MaxPooling2D((3, 3), strides=(2, 2), padding='same')(x)
	x = layers.add([x, r])

	r = Conv2D(728, (1, 1), strides=(2, 2), padding='same', use_bias=False)(x)
	r = BatchNormalization()(r)

	x = Activation('relu')(x)
	x = SeparableConv2D(728, (3, 3), padding='same', use_bias=False)(x)
	x = BatchNormalization()(x)
	x = Activation('relu')(x)
	x = SeparableConv2D(728, (3, 3), padding='same', use_bias=False)(x)
	x = BatchNormalization()(x)

	x = MaxPooling2D((3, 3), strides=(2, 2), padding='same')(x)
	x = layers.add([x, r])

	for i in range(8):
		r = x
		prefix = 'block' + str(i + 5)

		x = Activation('relu')(x)
		x = SeparableConv2D(728, (3, 3), padding='same', use_bias=False)(x)
		x = BatchNormalization()(x)
		x = Activation('relu')(x)
		x = SeparableConv2D(728, (3, 3), padding='same', use_bias=False)(x)
		x = BatchNormalization()(x)
		x = Activation('relu')(x)
		x = SeparableConv2D(728, (3, 3), padding='same', use_bias=False)(x)
		x = BatchNormalization()(x)

		x = layers.add([x, r])

	r = Conv2D(1024, (1, 1), strides=(2, 2), padding='same', use_bias=False)(x)
	r = BatchNormalization()(r)

	x = Activation('relu')(x)
	x = SeparableConv2D(728, (3, 3), padding='same', use_bias=False)(x)
	x = BatchNormalization()(x)
	x = Activation('relu')(x)
	x = SeparableConv2D(1024, (3, 3), padding='same', use_bias=False)(x)
	x = BatchNormalization()(x)

	x = MaxPooling2D((3, 3), strides=(2, 2), padding='same')(x)
	x = layers.add([x, r])

	x = SeparableConv2D(1536, (3, 3), padding='same', use_bias=False)(x)
	x = BatchNormalization()(x)
	x = Activation('relu')(x)

	x = SeparableConv2D(2048, (3, 3), padding='same', use_bias=False)(x)
	x = BatchNormalization()(x)
	x = Activation('relu')(x)

	if n_outputs > 0:
		# x = GlobalAveragePooling2D()(x)
		# x = Dense(n_outputs, activation='softmax')(x)

		x = Flatten()(x)
		x = Dense(n_outputs, activation='linear')(x)

	return x

# -------------------------- INCEPTION --------------------------

def _inception_conv2d_batchnorm(x,filters, num_row, num_col, pad='same', stride=(1, 1)):
	x = Conv2D(filters, (num_row, num_col), strides=stride, padding=pad, use_bias=False)(x)
	x = BatchNormalization(scale=False)(x)
	x = Activation('relu')(x)
	return x

def _inception_module1(x, pool_filters):
	branch1 = _inception_conv2d_batchnorm(x, 64, 1, 1)

	branch2 = _inception_conv2d_batchnorm(x, 48, 1, 1)
	branch2 = _inception_conv2d_batchnorm(branch2, 64, 5, 5)

	branch3 = _inception_conv2d_batchnorm(x, 64, 1, 1)
	branch3 = _inception_conv2d_batchnorm(branch3, 96, 3, 3)
	branch3 = _inception_conv2d_batchnorm(branch3, 96, 3, 3)

	branch4 = AveragePooling2D((3, 3), strides=(1, 1), padding='same')(x)
	branch4 = _inception_conv2d_batchnorm(branch4, pool_filters, 1, 1)
	x = layers.concatenate([branch1, branch2, branch3, branch4])

	return x

def _inception_module2(x, b2_filters, b3_filters):
	branch1 = _inception_conv2d_batchnorm(x, 192, 1, 1)

	branch2 = _inception_conv2d_batchnorm(x, b2_filters, 1, 1)
	branch2 = _inception_conv2d_batchnorm(branch2, b2_filters, 1, 7)
	branch2 = _inception_conv2d_batchnorm(branch2, 192, 7, 1)

	branch3 = _inception_conv2d_batchnorm(x, b3_filters, 1, 1)
	branch3 = _inception_conv2d_batchnorm(branch3, b3_filters, 7, 1)
	branch3 = _inception_conv2d_batchnorm(branch3, b3_filters, 1, 7)
	branch3 = _inception_conv2d_batchnorm(branch3, b3_filters, 7, 1)
	branch3 = _inception_conv2d_batchnorm(branch3, 192, 1, 7)

	branch4 = AveragePooling2D((3, 3), strides=(1, 1), padding='same')(x)
	branch4 = _inception_conv2d_batchnorm(branch4, 192, 1, 1)
	x = layers.concatenate([branch1, branch2, branch3, branch4])

	return x

def inception(input_node, n_outputs=0):

	x = _inception_conv2d_batchnorm(input_node, 32, 3, 3, strides=(2, 2), padding='valid')
	x = _inception_conv2d_batchnorm(x, 32, 3, 3, padding='valid')
	x = _inception_conv2d_batchnorm(x, 64, 3, 3)
	x = MaxPooling2D((3, 3), strides=(2, 2))(x)

	x = _inception_conv2d_batchnorm(x, 80, 1, 1, padding='valid')
	x = _inception_conv2d_batchnorm(x, 192, 3, 3, padding='valid')
	x = MaxPooling2D((3, 3), strides=(2, 2))(x)

	x = _inception_module1(x, 32)

	x = _inception_module1(x, 64)

	x = _inception_module1(x, 64)

	branch1 = _inception_conv2d_batchnorm(x, 384, 3, 3, strides=(2, 2), padding='valid')
	branch2 = _inception_conv2d_batchnorm(x, 64, 1, 1)
	branch2 = _inception_conv2d_batchnorm(branch2, 96, 3, 3)
	branch2 = _inception_conv2d_batchnorm(branch2, 96, 3, 3, strides=(2, 2), padding='valid')
	branch3 = MaxPooling2D((3, 3), strides=(2, 2))(x)
	x = layers.concatenate([branch1, branch2, branch3])

	x = _inception_module2(x, 128, 128)

	x = _inception_module2(x, 160, 160)

	x = _inception_module2(x, 160, 160)

	branch1 = _inception_conv2d_batchnorm(x, 192, 1, 1)
	branch2 = _inception_conv2d_batchnorm(x, 192, 1, 1)
	branch2 = _inception_conv2d_batchnorm(branch2, 192, 1, 7)
	branch2 = _inception_conv2d_batchnorm(branch2, 192, 7, 1)
	branch3 = _inception_conv2d_batchnorm(x, 192, 1, 1)
	branch3 = _inception_conv2d_batchnorm(branch3, 192, 7, 1)
	branch3 = _inception_conv2d_batchnorm(branch3, 192, 1, 7)
	branch3 = _inception_conv2d_batchnorm(branch3, 192, 7, 1)
	branch3 = _inception_conv2d_batchnorm(branch3, 192, 1, 7)
	branch4 = AveragePooling2D((3, 3), strides=(1, 1), padding='same')(x)
	branch4 = _inception_conv2d_batchnorm(branch4, 192, 1, 1)
	x = layers.concatenate([branch1, branch2, branch3, branch4])

	branch1 = _inception_conv2d_batchnorm(x, 192, 1, 1)
	branch1 = _inception_conv2d_batchnorm(branch1, 320, 3, 3, strides=(2, 2), padding='valid')
	branch2 = _inception_conv2d_batchnorm(x, 192, 1, 1)
	branch2 = _inception_conv2d_batchnorm(branch2, 192, 1, 7)
	branch2 = _inception_conv2d_batchnorm(branch2, 192, 7, 1)
	branch2 = _inception_conv2d_batchnorm(branch2, 192, 3, 3, strides=(2, 2), padding='valid')
	branch3 = MaxPooling2D((3, 3), strides=(2, 2))(x)
	x = layers.concatenate([branch1, branch2, branch3])

	for i in range(2):
		branch1 = _inception_conv2d_batchnorm(x, 320, 1, 1)
		branch2 = _inception_conv2d_batchnorm(x, 384, 1, 1)
		branch2_1 = _inception_conv2d_batchnorm(branch2, 384, 1, 3)
		branch2_2 = _inception_conv2d_batchnorm(branch2, 384, 3, 1)
		branch2 = layers.concatenate([branch2_1, branch2_2])
		branch3 = _inception_conv2d_batchnorm(x, 448, 1, 1)
		branch3 = _inception_conv2d_batchnorm(branch3, 384, 3, 3)
		branch3_1 = _inception_conv2d_batchnorm(branch3, 384, 1, 3)
		branch3_2 = _inception_conv2d_batchnorm(branch3, 384, 3, 1)
		branch3 = layers.concatenate([branch3_1, branch3_2])
		branch4 = AveragePooling2D((3, 3), strides=(1, 1), padding='same')(x)
		branch4 = _inception_conv2d_branch1batchnorm(branch4, 192, 1, 1)
		x = layers.concatenate([branch1, branch2, branch3, branch4])

	if n_outputs > 0:
		# x = GlobalAveragePooling2D()(x)
		# x = Dense(classes, activation='softmax')(x)

		x = Flatten()(x)
		x = Dense(n_outputs, activation='linear')(x)

	return x

# -------------------------- SQUEEZENET --------------------------

def _squeezenet_module(x, squeeze=16, expand=64):
	x = Conv2D(squeeze, (1, 1), activation='relu', padding='valid')(x)

	a = Conv2D(expand, (1, 1), activation='relu', padding='valid')(x)
	b = Conv2D(expand, (3, 3), activation='relu', padding='same')(x)

	x = layers.concatenate([a, b])

	return x

def squeezenet(input_node, n_outputs=0):

	x = Conv2D(64, (3, 3), strides=(2, 2), activation='relu', padding='valid')(input_node)
	x = MaxPooling2D(pool_size=(3, 3), strides=(2, 2))(x)

	x = _squeezenet_module(x, squeeze=16, expand=64)
	x = _squeezenet_module(x, squeeze=16, expand=64)
	x = MaxPooling2D(pool_size=(3, 3), strides=(2, 2))(x)

	x = _squeezenet_module(x, squeeze=32, expand=128)
	x = _squeezenet_module(x, squeeze=32, expand=128)
	x = MaxPooling2D(pool_size=(3, 3), strides=(2, 2))(x)

	x = _squeezenet_module(x, squeeze=48, expand=192)
	x = _squeezenet_module(x, squeeze=48, expand=192)
	x = _squeezenet_module(x, squeeze=64, expand=256)
	x = _squeezenet_module(x, squeeze=64, expand=256)
	x = Dropout(0.5)(x)

	if n_outputs > 0:
		# x = Conv2D(n_outputs, (1, 1), activation='relu', padding='valid')(x)
		# x = GlobalAveragePooling2D()(x)
		# x = Activation('softmax')(x)

		x = Flatten()(x)
		x = Dense(n_outputs, activation='linear')(x)

	return x

# -------------------------- FRCNN --------------------------

def rpn(input_node, n_anchors):

	x = Conv2D(512, (3, 3), padding='same', activation='relu', kernel_initializer='normal')(input_node)

	x_class = Conv2D(n_anchors, (1, 1), activation='sigmoid', kernel_initializer='uniform', name="rpn_cls")(x)
	x_regr  = Conv2D(n_anchors * 4, (1, 1), activation='linear', kernel_initializer='zero', name="rpn_reg")(x)

	return [x_class, x_regr]

def regressor(input_node, input_rois, n_outputs, num_rois):
	pooling_regions = 9
	shared_ft_maps = 512
	input_shape = (num_rois, pooling_regions, pooling_regions, shared_ft_maps)
	x = RoiPoolingConv(pooling_regions, num_rois)([input_node, input_rois])

	# x = Conv2D(128, (3, 3), input_shape=input_shape, activation='relu', kernel_initializer='normal', name="regressor_tl_in")(x)
	# x = Conv2D(64, (1, 1), activation='relu', kernel_initializer='normal')(x)
	# x = Dropout(0.25)(x)
	
	x = TimeDistributed(Flatten())(x)
	x = TimeDistributed(Dense(256, activation='linear', name="dense2561"))(x)
	x = TimeDistributed(Dense(512, activation='linear', name="dense512"))(x)
	x = TimeDistributed(Dense(256, activation='linear', name="dense2562"))(x)
	x = TimeDistributed(Dense(n_outputs, activation='linear', name="dense_out"))(x)

	return x