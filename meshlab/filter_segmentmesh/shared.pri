# This is the common include for all the plugins

include (general.pri)
#VCGDIR   = "$$MESHLAB_DIR"/vcglib
#EIGENDIR = $$VCGDIR/eigenlib

TEMPLATE      = lib
CONFIG       += plugin
QT += opengl
QT += xml
QT += xmlpatterns
QT += script

linux-g++-64:LIBS += -L"$$MESHLAB_DIR"/src/distrib -lGL -lGLU #-lcommon

# uncomment to try Eigen
# DEFINES += VCG_USE_EIGEN
# CONFIG += warn_off

INCLUDEPATH  *= "$$MESHLAB_DIR"/src $$VCGDIR $$EIGENDIR $$GLEWDIR/include
DEPENDPATH += "$$MESHLAB_DIR"/src $$VCGDIR

CONFIG(release,debug | release){
# Uncomment the following line to disable assert in mingw
#DEFINES += NDEBUG
 }

DESTDIR       = ../plugins
#DESTDIR = /home/jurij/src/meshlab/build-meshlab_full-Desktop_gcc-Debug/distrib/plugins/

# uncomment in you local copy only in emergency cases.
# We should never be too permissive
# win32-g++:QMAKE_CXXFLAGS += -fpermissive

contains(TEMPLATE,lib) {
   CONFIG(debug, debug|release) {
      unix:TARGET = $$member(TARGET, 0)_debug
      else:TARGET = $$member(TARGET, 0)d
   }
}
