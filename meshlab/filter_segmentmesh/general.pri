# this is the common include for anything compiled inside the meshlab pro


# This is the main coord type inside meshlab
# it can be double or float according to user needs.
DEFINES += MESHLAB_SCALAR=float

VCGDIR   = "$$MESHLAB_DIR"/vcglib
EIGENDIR = $$VCGDIR/eigenlib
GLEWDIR = "$$MESHLAB_DIR"/src/external/glew-1.7.0

CONFIG += c++11


linux-g++:QMAKE_CXXFLAGS+=-Wno-unknown-pragmas
