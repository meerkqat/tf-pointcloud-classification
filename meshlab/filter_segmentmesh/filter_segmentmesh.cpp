/****************************************************************************
* MeshLab                                                           o o     *
* A versatile mesh processing toolbox                             o     o   *
*                                                                _   O  _   *
* Copyright(C) 2005                                                \/)\/    *
* Visual Computing Lab                                            /\/|      *
* ISTI - Italian National Research Council                           |      *
*                                                                    \      *
* All rights reserved.                                                      *
*                                                                           *
* This program is free software; you can redistribute it and/or modify      *
* it under the terms of the GNU General Public License as published by      *
* the Free Software Foundation; either version 2 of the License, or         *
* (at your option) any later version.                                       *
*                                                                           *
* This program is distributed in the hope that it will be useful,           *
* but WITHOUT ANY WARRANTY; without even the implied warranty of            *
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the             *
* GNU General Public License (http://www.gnu.org/licenses/gpl.txt)          *
* for more details.                                                         *
*                                                                           *
****************************************************************************/
#include "filter_segmentmesh.h"
#include <vcg/complex/algorithms/create/platonic.h>
#include <vcg/complex/algorithms/point_sampling.h>
#include <vcg/complex/algorithms/smooth.h>
#include <vcg/math/gen_normal.h>

#include <cstdio>
#include <iostream>
#include <memory>
#include <stdexcept>
#include <string>
#include <array>

using namespace vcg;
using namespace tri;

std::string exec(const char* cmd) {
    std::array<char, 128> buffer;
    std::string result;
    std::shared_ptr<FILE> pipe(popen(cmd, "r"), pclose);
    if (!pipe) throw std::runtime_error("popen() failed!");
    while (!feof(pipe.get())) {
        if (fgets(buffer.data(), 128, pipe.get()) != NULL)
            result += buffer.data();
    }
    return result;
}

// Constructor usually performs only two simple tasks of filling the two lists
//  - typeList: with all the possible id of the filtering actions
//  - actionList with the corresponding actions. If you want to add icons to your filtering actions you can do here by construction the QActions accordingly
FilterSegmentMesh::FilterSegmentMesh()
{
    typeList << SM_DEP << SM_VOX;

    foreach(FilterIDType tt , types())
        actionList << new QAction(filterName(tt), this);
}

QString FilterSegmentMesh::filterName(FilterIDType filterId) const
{
    switch(filterId) {
        case SM_DEP : return QString("SegmentMesh depth images");
        case SM_VOX : return QString("SegmentMesh voxelization");
        default : assert(0);
    }
}

// Info() must return the longer string describing each filtering action
// (this string is used in the About plugin dialog)
QString FilterSegmentMesh::filterInfo(FilterIDType filterId) const
{
    switch(filterId) {
        case SM_DEP : return QString("Segment objects from mesh with CNN using depth images of scene.");
        case SM_VOX : return QString("Segment objects from mesh with CNN using scene voxelization.");
        default : assert(0);
    }
}

// This function define the needed parameters for each filter. Return true if the filter has some parameters
// it is called every time, so you can set the default value of parameters according to the mesh
// For each parmeter you need to define,
// - the name of the parameter,
// - the string shown in the dialog
// - the default value
// - a possibly long string describing the meaning of that parameter (shown as a popup help in the dialog)
void FilterSegmentMesh::initParameterSet(QAction *action, MeshModel & /*m*/, RichParameterSet & parlst)
{
    parlst.addParam(new RichFloat("radius",1,"Radius","Radius of the sphere"));
    parlst.addParam(new RichInt("subdiv",3,"Subdiv. Level","Number of the recursive subdivision of the surface. Default is 3 (a sphere approximation composed by 1280 faces).<br>"
                                "Admitted values are in the range 0 (an icosahedron) to 8 (a 1.3 MegaTris approximation of a sphere)"));

    /*
    case CR_FITPLANE:
        parlst.addParam(new RichFloat("extent", 1.0, "Extent (with respect to selection)", "Howe large is the plane, with respect to the size of the selction: 1.0 means as large as the selection, 1.1 means 10% larger thena the selection"));
        parlst.addParam(new RichInt("subdiv", 3, "Plane XY subivisions", "Subdivision steps of plane borders"));
        parlst.addParam(new RichBool("hasuv", false, "UV parametrized", "The created plane has an UV parametrization"));
        parlst.addParam(new RichEnum("orientation", 0,
            QStringList() << "quasi-Straight Fit" << "Best Fit" << "XZ Parallel" << "YZ Parallel" << "YX Parallel",
            tr("Plane orientation"),
            tr("Orientation:"
            "<b>quasi-Straight Fit</b>: The fitting plane will be oriented (as much as possible) straight with the axeses.<br>"
            "<b>Best Fit</b>: The fitting plane will be oriented and sized trying to best fit to the selected area.<br>"
            "<b>-- Parallel</b>: The fitting plane will be oriented with a side parallel with the chosen plane. WARNING: do not use if the selection is exactly parallel to a plane.<br>"
            )));
        break;
    */

}

// The Real Core Function doing the actual mesh processing.
bool FilterSegmentMesh::applyFilter(QAction *filter, MeshDocument &md, RichParameterSet & par, CallBackPos * /*cb*/)
{

    string output = exec("python native.py");
    cout << output << endl;

	MeshModel *currM = md.mm();
	MeshModel *m;

    switch(ID(filter)) {
        case SM_DEP : printf("you chose dep\n"); break;
        case SM_VOX : printf("you chose vox\n"); break;
        default : return false;
    }
    /*
	case CR_FITPLANE:
	{
		Box3m selBox; //boundingbox of the selected vertices
		std::vector< Point3m > selected_pts; //copy of selected vertices, for plane fitting

		if (currM == NULL)
		{
			errorMessage = "No mesh layer selected"; 
			return false;
		}

		if (currM->cm.svn == 0 && currM->cm.sfn == 0) // if no selection, fail
		{
			errorMessage = "No selection";
			return false;
		}

		m = md.addNewMesh("", "Fitted Plane");

		if (currM->cm.svn == 0 || currM->cm.sfn != 0)
		{
			tri::UpdateSelection<CMeshO>::VertexClear(currM->cm);
			tri::UpdateSelection<CMeshO>::VertexFromFaceLoose(currM->cm);
		}

		Point3m Naccum = Point3m(0.0, 0.0, 0.0);
		for (CMeshO::VertexIterator vi = currM->cm.vert.begin(); vi != currM->cm.vert.end(); ++vi)
		if (!(*vi).IsD() && (*vi).IsS())
		{
			Point3m p = (*vi).P();
			selBox.Add(p);
			selected_pts.push_back(p);
			Naccum = Naccum + (*vi).N();
		}
		Log("Using %i vertexes to build a fitting  plane", int(selected_pts.size()));
		Plane3m plane;
		FitPlaneToPointSet(selected_pts, plane);
		plane.Normalize();
		// check if normal of the interpolated plane is coherent with average normal of the used points, otherwise, flip
		// i do this because plane fitter does not take in account source noramls, and a fliped fit is terrible to see
		Naccum = (Naccum / (CMeshO::ScalarType)selected_pts.size()).Normalize();
		if ((plane.Direction() * Naccum) < 0.0)
			plane.Set(-plane.Direction(), -plane.Offset());

		float errorSum = 0;
		for (size_t i = 0; i < selected_pts.size(); ++i)
			errorSum += fabs(SignedDistancePlanePoint(plane, selected_pts[i]));
		Log("Fitting Plane avg error is %f", errorSum / float(selected_pts.size()));
		Log("Fitting Plane normal is [%f, %f, %f]", plane.Direction().X(), plane.Direction().Y(), plane.Direction().Z());
		Log("Fitting Plane offset is %f", plane.Offset());

		// find center of selection on plane
		Point3m centerP;
		for (size_t i = 0; i < selected_pts.size(); ++i)
		{
			centerP += plane.Projection(selected_pts[i]);
		}
		centerP /= selected_pts.size();
		Log("center [%f, %f, %f]", centerP.X(), centerP.Y(), centerP.Z());

		// find horizontal and vertical axis
		Point3m dirH, dirV;

		int orientation = par.getEnum("orientation");

		if (orientation == 0)
		{
			if ((plane.Direction().X() <= plane.Direction().Y()) && (plane.Direction().X() <= plane.Direction().Z()))
				dirH = Point3m(1.0, 0.0, 0.0) ^ plane.Direction();
			else if ((plane.Direction().Y() <= plane.Direction().X()) && (plane.Direction().Y() <= plane.Direction().Z()))
				dirH = Point3m(0.0, 1.0, 0.0) ^ plane.Direction();
			else
				dirH = Point3m(0.0, 0.0, 1.0) ^ plane.Direction();

			dirH.Normalize();
			dirV = dirH ^ plane.Direction();
			dirV.Normalize();
		}
		else if (orientation == 1)
		{
			Matrix33m cov;
			vector<Point3m> PtVec;
			for (size_t i = 0; i < selected_pts.size(); ++i)
				PtVec.push_back(plane.Projection(selected_pts[i]));

			cov.Covariance(PtVec, centerP);
			Matrix33f eigenvecMatrix;
			Point3f eigenvecVector;
			Eigen::Matrix3d em;
			cov.ToEigenMatrix(em);
			Eigen::SelfAdjointEigenSolver<Eigen::Matrix3d> eig(em);
			Eigen::Vector3d c_val = eig.eigenvalues();
			Eigen::Matrix3d c_vec = eig.eigenvectors();

			eigenvecMatrix.FromEigenMatrix(c_vec);
			eigenvecVector.FromEigenVector(c_val);

			// max eigenvector is best horizontal axis, but is not guarantee is orthogonal to plane normal, so
			// I use eigenvector ^ plane direction and assign it to vertical plane axis
			if ((eigenvecVector[0]<=eigenvecVector[1]) && (eigenvecVector[0]<=eigenvecVector[2]))
				dirV = Point3m(eigenvecMatrix[0][0], eigenvecMatrix[0][1], eigenvecMatrix[0][2]) ^ plane.Direction();
			if ((eigenvecVector[1]<=eigenvecVector[0]) && (eigenvecVector[1]<=eigenvecVector[2]))
				dirV = Point3m(eigenvecMatrix[1][0], eigenvecMatrix[1][1], eigenvecMatrix[1][2]) ^ plane.Direction();
			else
				dirV = Point3m(eigenvecMatrix[2][0], eigenvecMatrix[2][1], eigenvecMatrix[2][2]) ^ plane.Direction();

			dirV.Normalize();
			dirH = plane.Direction() ^ dirV;
			dirH.Normalize();
		}
		else if (orientation == 2)
		{
				dirH = Point3m(0.0, 1.0, 0.0) ^ plane.Direction();
				dirH.Normalize();
				dirV = dirH ^ plane.Direction();
				dirV.Normalize();

		}
		else if (orientation == 3)
		{
				dirH = Point3m(1.0, 0.0, 0.0) ^ plane.Direction();
				dirH.Normalize();
				dirV = dirH ^ plane.Direction();
				dirV.Normalize();
		}
		else if (orientation == 4)
		{

				dirH = Point3m(0.0, 0.0, 1.0) ^ plane.Direction();
				dirH.Normalize();
				dirV = dirH ^ plane.Direction();
				dirV.Normalize();
		}

		// hotfix for unlikely case where the fitting is perfecrly parallel to a plane
		if (orientation >= 2 )
		{
			if (Point3m(0.0, 1.0, 0.0) * plane.Direction() == 1.0)
			{
				dirH = Point3m(1.0, 0.0, 0.0);
				dirV = Point3m(0.0, 0.0, 1.0);
			}
			if (Point3m(0.0, 0.0, 1.0) * plane.Direction() == 1.0)
			{
				dirH = Point3m(1.0, 0.0, 0.0);
				dirV = Point3m(0.0, 1.0, 0.0);
			}
			if (Point3m(1.0, 0.0, 0.0) * plane.Direction() == 1.0)
			{
				dirH = Point3m(0.0, 1.0, 0.0);
				dirV = Point3m(0.0, 0.0, 1.0);
			}
		}

		Log("H [%f, %f, %f]", dirH.X(), dirH.Y(), dirH.Z());
		Log("V [%f, %f, %f]", dirV.X(), dirV.Y(), dirV.Z());


		// find extent
		float dimH = -1000000;
		float dimV = -1000000;
		for (size_t i = 0; i < selected_pts.size(); ++i)
		{
			Point3m pp = plane.Projection(selected_pts[i]);
			float distH = fabs(((pp - centerP) * dirH));
			float distV = fabs(((pp - centerP) * dirV));

			if (distH > dimH)
				dimH = distH;
			if (distV > dimV)
				dimV = distV;
		}
		float exScale = par.getFloat("extent");
		dimV = dimV * exScale;
		dimH = dimH * exScale;
		Log("extent on plane [%f, %f]", dimV, dimH);

		int vertNum = par.getInt("subdiv") + 1;
		if (vertNum <= 1) vertNum = 2;
		int numV, numH;
		numV = numH = vertNum;

		// UV vector, just in case
		float *UUs, *VVs;
		UUs = new float[numH*numV];
		VVs = new float[numH*numV];

		int vind = 0;
		for (int ir = 0; ir < numV; ir++)
		for (int ic = 0; ic < numH; ic++)
			{
				Point3m newP = (centerP + (dirV * -dimV) + (dirH * -dimH)); 
				newP = newP + (dirH * ic * (2.0 * dimH / (numH-1))) + (dirV * ir * (2.0 * dimV / (numV-1)));
				tri::Allocator<CMeshO>::AddVertex(m->cm, newP, plane.Direction());
				UUs[vind] = ic * (1.0 / (numH - 1));
				VVs[vind] = ir * (1.0 / (numV - 1));
				vind++;
			}
		
		FaceGrid(m->cm, numH, numV);

		bool hasUV = par.getBool("hasuv");
		if (hasUV)
		{
			m->updateDataMask(MeshModel::MM_WEDGTEXCOORD);

			CMeshO::FaceIterator fi;
			for (fi = m->cm.face.begin(); fi != m->cm.face.end(); ++fi)
			{
				for (int i = 0; i<3; ++i)
				{
					int vind = (*fi).V(i)->Index();
					(*fi).WT(i).U() = UUs[vind];
					(*fi).WT(i).V() = VVs[vind];
				}
			}
		}
		delete[] UUs;	// delete temporary UV storage
		delete[] VVs;

	} break;
    */

    int rec = par.getInt("subdiv");
    float radius = par.getFloat("radius");
    m = md.addNewMesh("", this->filterName(ID(filter)));
    m->cm.face.EnableFFAdjacency();
    m->updateDataMask(MeshModel::MM_FACEFACETOPO);
    assert(tri::HasPerVertexTexCoord(m->cm) == false);
    tri::Sphere<CMeshO>(m->cm,rec);
    tri::UpdatePosition<CMeshO>::Scale(m->cm,radius);


    tri::UpdateBounding<CMeshO>::Box(m->cm);
    tri::UpdateNormal<CMeshO>::PerVertexNormalizedPerFaceNormalized(m->cm);
    return true;
}

 MeshFilterInterface::FilterClass FilterSegmentMesh::getClass(QAction *a)
{
    switch(ID(a))
    {
        case SM_DEP:
        case SM_VOX:
            return MeshFilterInterface::MeshCreation;
            //return MeshFilterInterface::Measure;
            break;
        default:
            assert(0);
        return MeshFilterInterface::Generic;
    }
}

QString FilterSegmentMesh::filterScriptFunctionName( FilterIDType filterID )
 {
	switch(filterID)
	{
        case SM_DEP : return QString("depthimages");
        case SM_VOX : return QString("voxelization");
        default : assert(0);
    }
 }


MESHLAB_PLUGIN_NAME_EXPORTER(FilterSegmentMesh)
