include (shared.pri)

MESHLAB_DIR = /home/jurij/src/meshlab
MESHLAB_BUILD_DIR = build-meshlab_full-Desktop_gcc-Debug

INCLUDEPATH += "$$MESHLAB_DIR"/src

HEADERS       += filter_segmentmesh.h
SOURCES       += filter_segmentmesh.cpp
OTHER_FILES	  += native.py

TARGET         = filter_segmentmesh

# copy built plugin + native scripts to meshlab folder
copyplugin.commands = $(COPY_DIR) $$DESTDIR/* $$MESHLAB_DIR/$$MESHLAB_BUILD_DIR/distrib/plugins
copynative.commands = $(COPY_DIR) $$PWD/native.py $$MESHLAB_DIR/$$MESHLAB_BUILD_DIR/distrib

first.depends = $(first) copyplugin copynative
export(first.depends)
export(copyplugin.commands)
export(copynative.commands)
QMAKE_EXTRA_TARGETS += first copyplugin copynative
