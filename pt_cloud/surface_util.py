import math
import json
import numpy as np

from pt_cloud_consts import *
import pt_cloud_gen as gen

# ax+by+cz+d = 0
def plane_parametrization(p1, p2, p3):
	n_digits = 3

	p1 = np.array(p1)
	p2 = np.array(p2)
	p3 = np.array(p3)

	v1 = p3-p1
	v2 = p2-p1

	n = np.cross(v1, v2)
	d = -np.dot(n,p1)

	return [round(n[0],n_digits),round(n[1],n_digits),round(n[2],n_digits),round(d,n_digits)]


def import_pointcloud():

	f=open(DATA_DIR+"scenes.json", "r"); 
	scenes = json.load(f);

	all_scenes = {}
	for s in scenes:
		rects = []	
		for o in scenes[s]:
			w = scenes[s][o]["w"]
			h = scenes[s][o]["h"]

			rect = gen.translate(gen.rotate([[w/2.0,h/2.0,0.0],[w/2.0,-h/2.0,0.0],[-w/2.0,h/2.0,0.0],[-w/2.0,-h/2.0,0.0]],scenes[s][o]["rx"],scenes[s][o]["ry"],scenes[s][o]["rz"]),scenes[s][o]["tx"],scenes[s][o]["ty"],scenes[s][o]["tz"])
			rects.append(rect)
		all_scenes[s] = rects

	return all_scenes

def parametrize_scenes(pt_cloud_dict):
	ground_truths = {}
	for scene in pt_cloud_dict:
		rects = []
		for rect in pt_cloud_dict[scene]:
			rects.append(plane_parametrization(rect[0],rect[1],rect[2]))
		ground_truths[scene] = rects

	return ground_truths

# could potentially cause division by 0 if surface exactly parallel to any axis, but that's extremely unlikely
def export_surface(a,b,c,d,fname):
	f = open(DATA_DIR+fname+".obj", "w")
	f.write("v %f %f %f\n" % (-d/a, 0.0, 0.0))
	f.write("v %f %f %f\n" % (0.0, -d/b, 0.0))
	f.write("v %f %f %f\n" % (0.0, 0.0, -d/c))
	f.write("vn %f %f %f\n" % (a, b, c))
	f.write("f 1//1 2//1 3//1\n")
	f.close()


# n_scenes, mn_objects, mx_objects, mn_points, mx_points, mn_size, mx_size, mn_delta, mx_delta, mn_dist, mx_dist, data_dir
gen.generate_scenes(2, 1, 1, 500, 500, 10, 50, 0, 0, 10, 100, ["rect"], DATA_DIR)

# only 1 surface per scene
paramd_dict = parametrize_scenes(import_pointcloud())
for s in paramd_dict:
	export_surface(paramd_dict[s][0][0], paramd_dict[s][0][1], paramd_dict[s][0][2], paramd_dict[s][0][3], s+"_surface")