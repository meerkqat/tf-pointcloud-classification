import json
from itertools import chain
from sys import float_info
from sys import argv
import numpy as np

import h5py

from pt_cloud_consts import *
import pt_cloud_gen as gen
import pt_cloud_voxel as vox
import pt_cloud_depth as dep

# running from command line
# ~$ python pt_cloud_run.py [data_dir_path] [genX] [vox] [dep] [rad] [pos] [rot] [side] [superquad] [preproc]

# data_dir_path : where to dump the generated files

# the rest are switches, that make something happen when they are included
# genX : generate fresh data instead of using existing data in data_dir, 
#		 X should be replaced with an integer (how many scenes to generate) or it can be 
# 		 ommited (gen defaults to gen100)
# vox : generate voxel representation of data
# dep : generate depth images
# rad : when exporting to csv include radiuses of spheres in the scene at the end of each line (1 scene = 1 line)
# pos : when exporting to csv include positions of objects in the scene at the end of each line
# rot : when exporting to csv include rotations of objects in the scene at the end of each line
# side : when exporting to csv include dimensions of boxes in the scene at the end of each line
# superquad : when exporting to csv include superquadric parameters at the end of each line
# preproc : do a preprocessing step with depth images - get the one with best separation

# generally unimportant what order the params are in; the switches are processed first, then 
# the first remaining param gets used for data_dir, the rest of the params are ignored 
# (also prints ignored params just FYI)

# changing how/what the dataset generates still requires tweaking code below

# ------ EDITABLE PARAMS ------

# num of objects per scene
mn_objects = 1
mx_objects = 3
# num of points per object
mn_points = 1500
mx_points = 2000
# object size
mn_size = 100
mx_size = 200
# object fuzziness
mn_delta = 0
mx_delta = 0.5
# object translation from coord sys origin
mn_dist = 20
mx_dist = 250
# objects to gen  ["rect","box","circle","sphere","tube"]
objects = ["superquad"]

EXPORT_IMAGES = True

# ------ /EDITABLE PARAMS ------



export_params = []

DO_GEN = False
DO_VOX = False
DO_DEPTH = False
DO_PREPROC = False

data_dir = "data/tmp/"

n_scenes = 100

#  ----------------- Load cmdline params -----------------

args = argv[1:]

try:
	for p in args:
		if p.startswith("gen"):
			args.remove(p)
			DO_GEN = True
			n = p[3:]
			if len(n) > 0:
				n_scenes = int(n)
except:
	DO_GEN = False
try:
	args.remove("vox")
	DO_VOX = True
except:
	DO_VOX = False
try:
	args.remove("dep")
	DO_DEPTH = True
except:
	DO_DEPTH = False

try:
	args.remove("rad")
	export_params.append("r")
except:
	pass
try:
	args.remove("side")
	export_params.append("w")
	export_params.append("h")
	export_params.append("d")
except:
	pass
try:
	args.remove("pos")
	export_params.append("tx")
	export_params.append("ty")
	export_params.append("tz")
except:
	pass
try:
	args.remove("rot")
	export_params.append("rx")
	export_params.append("ry")
	export_params.append("rz")
except:
	pass
try:
	args.remove("superquad")
	export_params.append("A")
	export_params.append("B")
	export_params.append("C")
	export_params.append("r")
	export_params.append("s")
	export_params.append("t")
except:
	pass
try:
	args.remove("preproc")
	DO_PREPROC = True
except:
	DO_PREPROC = False
try:
	# just in case append /
	data_dir = args.pop(0)+"/"
except:
	pass

if len(args) > 0:
	print "Unused params:",str(args)

#  -----------------  -----------------  -----------------

assert_dir(data_dir)

"""
Finds the view that best separates objects in the scene.
Currently just sums the whole image and assumes the best separation is the one where the objects
cover as much of the image as possible. 

@param
depth_images : depth_images[image_h[image_w[float...]]...] : array of depth images of a single scene

@ return 
image_h[image_w[float...]] : depth image with the best object separation view
"""
def best_view(depth_images):
	scores = []
	for view,bboxes in depth_images:
		scores.append(sum(sum(view)))

	return depth_images[np.argmin(scores)]

# converts each scene into a 1D vector (from 3D) and appends the parameters at the end of that vector,
# concats the vectors into a 2D matrix and writes the matrix to a file
def vox_csv(vox_scenes, desc, params, data_dir=DATA_DIR, normalize=False):
	out_data = []
	for scene_id,scene in vox_scenes.iteritems():
		scene = np.array(scene)
		if normalize:
			scene[scene > 0] = 1

		scene = np.reshape(scene, (-1))

		out_params = []
		for obj in desc[scene_id].keys():
			for p in params:
				out_params.append(desc[scene_id][obj][p])

		out_data.append(np.append(scene, out_params))
	
	print "Saving scenes data to", data_dir+"scenes_vox.h5\n"
	with h5py.File(data_dir+"scenes_vox.h5", 'w') as hf:
		hf.create_dataset("scenes",  data=out_data)

# converts each image of a scene into a 1D vector (from 2D), appends the parameters at the end of that vector,
# concats the vectors into a 2D matrix and writes the matrix to a file
def depth_csv(depth_images, params, data_dir=DATA_DIR):
	fnamegen = lambda ddir,sid,idx: str(ddir)+str(sid)+"_depth"+str(idx)+".png"

	out_data = []
	f = open(data_dir+"scenes_bboxes.csv", "w")
	for scene_id,images in depth_images.iteritems():
		print "csv-ing",scene_id

		for i in xrange(len(images)):
			out_data.append(images[i][0].reshape(-1))

			# params are part of cls - kinda hacky, but it works
			for bbox in images[i][1]:
				coords = bbox[0]
				cls = bbox[1]
				f.write("%s,%d,%d,%d,%d,%s\n" % (fnamegen(data_dir, scene_id, i),coords[0][0],coords[0][1],coords[1][0],coords[1][1],cls))
	
	print "Saving scenes data to", data_dir+"scenes.h5\n"
	with h5py.File(data_dir+"scenes.h5", 'w') as hf:
		hf.create_dataset("scenes",  data=out_data)
	f.close()


scenes = {}

if DO_GEN:
	#(n_scenes, mn_objects, mx_objects, mn_points, mx_points, mn_size, mx_size, mn_delta, mx_delta, mn_dist, mx_dist, objects=["rect","box","circle","sphere","tube"], save_dir="data/", save_csv=False)
	scenes = gen.generate_scenes(n_scenes, mn_objects, mx_objects, mn_points, mx_points, mn_size, mx_size, mn_delta, mx_delta, mn_dist, mx_dist, objects, data_dir)

if DO_DEPTH:
	#(pplane_offset=PPLANE_OFFSET, focus_len=FOCUS_LEN, image_w=IMAGE_W, image_h=IMAGE_H, bins_w=BINS_W, bins_h=BINS_H, data_dir=DATA_DIR, export_images=False, place_wall=WALL_DIST, normalize_vals=255.0, data={})
	depth_images = dep.generate_depth_images_data(data_dir=data_dir, export_images=EXPORT_IMAGES, normalize_vals=-255, data=scenes)

	if DO_PREPROC:
		for scene_id,scene in depth_images.iteritems():
			depth_images[scene_id] = [best_view(scene)]

	depth_csv(depth_images, export_params, data_dir)


if DO_VOX:
	f=open(data_dir+"scenes.json", "r")
	desc = json.load(f)

	#(steps_x=STEPS_X, steps_y=STEPS_Y, steps_z=STEPS_Z, data_dir=DATA_DIR, export_obj=False, data={})
	vox_scenes = vox.voxelize_data(data_dir=data_dir, export_obj=False)
	vox_csv(vox_scenes, desc, export_params, data_dir, normalize=False)