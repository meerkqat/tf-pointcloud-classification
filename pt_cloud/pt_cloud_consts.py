from random import seed
from time import time
from os import path
from os import makedirs

##### GENERAL PARAMS #####

seed(time())
#seed(42)

# save generated files to this directory
def assert_dir(file_path):
	# make sure path is capped with / if it isn't; otherwise it treats the last part as file not dir
	directory = path.dirname(file_path+"/")
	if not path.exists(directory):
		makedirs(directory)

DATA_DIR = "data/"

##### VOXEL SCENE #####

# how many voxels per dimension in voxel representation
STEPS_X = 30
STEPS_Y = 30
STEPS_Z = 30

# from where to where the voxelization grid should be set up
# if any axis here <= 0, then the range for that axis is set to (min, max) of the object
VOX_MAX_X = -1
VOX_MAX_Y = -1
VOX_MAX_Z = -1

##### DEPTH IMAGE #####

# distance between coordinate system origin and projection plane
PPLANE_OFFSET = 350
# distance between projection plane and viewpoint 
# negative values put focal point between origin and proj. plane
# positive values put focal point behind proj. plane in relation to origin
FOCUS_LEN = 475
# distance from projection plane to "wall" behind scene
WALL_DIST = PPLANE_OFFSET*2

# dimensions of projection plane in coordinate system
IMAGE_W = 600
IMAGE_H = 600
# resolution of generated depth image
BINS_W = IMAGE_W
BINS_H = IMAGE_H
