import numpy as np

from pt_cloud_consts import *
import pt_cloud_gen as gen



"""
Finds the min/max bounds of a scene. Bounds can be set manually in pt_cloud_consts.

@param 
scene : scene[vertex[float,float,float]...] : the scene for which to find the bounds

@return 
[[float,float],[float,float],[float,float]] : array of min/max values [[minx,maxx],[miny,maxy],[minz,maxz]]
"""
def get_bounds(scene):
	scene_t = np.transpose(scene)
	rx = [-VOX_MAX_X, VOX_MAX_X] if VOX_MAX_X > 0 else [min(scene_t[0]),max(scene_t[0])]
	ry = [-VOX_MAX_Y, VOX_MAX_Y] if VOX_MAX_Y > 0 else [min(scene_t[1]),max(scene_t[1])]
	rz = [-VOX_MAX_Z, VOX_MAX_Z] if VOX_MAX_Z > 0 else [min(scene_t[2]),max(scene_t[2])]

	return [rx,ry,rz]

"""
Takes a pointcloud and converts it into a voxel representation. 

@param
scene : scene[vertex[float,float,float]...] : pointcloud of the scene
steps_x : int : how many voxels long the scene is in the x dimension
steps_y : int : how many voxels long the scene is in the y dimension
steps_z : int : how many voxels long the scene is in the z dimension

@return 
vox_scene_z[vox_scene_y[vox_scene_x[int...]...]...] : 3d matrix representing the voxelized scene. 
	Dimensions of the matrix are steps_z[steps_y[steps_x[]]]. The integer values in the matrix 
	represent how many points from the pointcloud coincide with the specific voxel.
"""
def voxelize_scene(scene, steps_x=STEPS_X, steps_y=STEPS_Y, steps_z=STEPS_Z):
	bound_x,bound_y,bound_z = get_bounds(scene)
	bound_x[1] = bound_x[1]-bound_x[0]
	bound_y[1] = bound_y[1]-bound_y[0]
	bound_z[1] = bound_z[1]-bound_z[0]
	# add a little bit of extra space, so that points on boundary don't overflow the matrix
	# TODO is this the best way to go about this? you're essentially inflating the whole dataset a bit
	div_x = (bound_x[1]*1.00001)/steps_x
	div_y = (bound_y[1]*1.00001)/steps_y
	div_z = (bound_z[1]*1.00001)/steps_z

	vox_scene = np.zeros((steps_z,steps_y,steps_x), dtype=np.int)

	for x,y,z in scene:
		idx_x = int((x-bound_x[0])/div_x)
		idx_y = int((y-bound_y[0])/div_y)
		idx_z = int((z-bound_z[0])/div_z)
		vox_scene[idx_z][idx_y][idx_x] += 1

	return vox_scene

"""
Exports voxel matrix to an .obj file

@param
vox_scene : vox_scene_z[vox_scene_y[vox_scene_x[int...]...]...] : 3d matrix representing the voxelized scene
bounds : [[float,float],[float,float],[float,float]] : coordinate bounds of voxel matrix
	[[minx,maxx],[miny,maxy],[minz,maxz]] (the matrix is a bunch of integers, not actual coordinates)
fname : string : name of the .obj file to export to
"""
def exportVoxObj(vox_scene, bounds, fname="scene_vox"):
	#vertices = []
	steps_x = len(vox_scene[0][0])
	steps_y = len(vox_scene[0])
	steps_z = len(vox_scene)

	f = open(fname+".obj", "w")
	for z in np.linspace(bounds[2][0],bounds[2][1],steps_z+1):
		for y in np.linspace(bounds[1][0],bounds[1][1],steps_y+1):
			for x in np.linspace(bounds[0][0],bounds[0][1],steps_x+1):
				#vertices.append([x,y,z])
				f.write("v %f %f %f\n" % (x, y, z))

	vtxs_per_row = steps_x+1
	vtxs_per_slice = vtxs_per_row*(steps_y+1)
	for z in xrange(steps_z):
		for y in xrange(steps_y):
			for x in xrange(steps_x):
				if vox_scene[z][y][x] > 0:
					v1 = x + vtxs_per_row*y + vtxs_per_slice*z +1 # obj indices start at 1 :(
					v2 = v1 + 1
					v3 = v1 + vtxs_per_row
					v4 = v3 + 1
					v5 = v1 + vtxs_per_slice
					v6 = v2 + vtxs_per_slice
					v7 = v3 + vtxs_per_slice
					v8 = v4 + vtxs_per_slice

					f.write("f %d %d %d %d\n" % (v1,v3,v4,v2))
					f.write("f %d %d %d %d\n" % (v5,v6,v8,v7))
					f.write("f %d %d %d %d\n" % (v1,v2,v6,v5))
					f.write("f %d %d %d %d\n" % (v2,v4,v8,v6))
					f.write("f %d %d %d %d\n" % (v4,v3,v7,v8))
					f.write("f %d %d %d %d\n" % (v3,v1,v5,v7))
	f.close()

"""
Returns voxel representation for all scenes in data directory

@param
steps_x : int : how many voxels long the scene is in the x dimension
steps_y : int : how many voxels long the scene is in the y dimension
steps_z : int : how many voxels long the scene is in the z dimension
data_dir : string : folder where data is located (.obj files)
export_obj : boolean : if true a .obj representation of the voxel matrices will be generated
data : {string:scenes[scene[vertex[float,float,float]...]...]} : dictionary of scene_id:scene. 
	Default is empty dict '{}' in which case it will attempt to read scenes from .obj files in the data directory

@return 
{string:vox_scene_z[vox_scene_y[vox_scene_x[int...]...]...]} : dictionary with keys denoting scene id and values being the voxel matrix
"""
def voxelize_data(steps_x=STEPS_X, steps_y=STEPS_Y, steps_z=STEPS_Z, data_dir=DATA_DIR, export_obj=False, data={}):
	print "Voxelizing data"

	assert_dir(data_dir)

	if len(data) == 0:
		print "Importing scenes"
		data = gen.importObj(data_dir, separate_objects=False)

	voxel_scenes = {}
	
	ctr = 0
	datalen = len(data.keys())

	for scene_id,scene in data.iteritems():
		ctr += 1
		print "Converting to voxels %s  %d/%d" % (scene_id,ctr, datalen)
		voxel_scenes[scene_id] = voxelize_scene(scene, steps_x, steps_y, steps_z)
	
	if export_obj:
		for scene_id,scene in data.iteritems():
			print "Exporting",scene_id,"to .obj"
			exportVoxObj(voxel_scenes[scene_id],get_bounds(scene), data_dir+scene_id+"_vox")

	print "Done\n"

	return voxel_scenes
