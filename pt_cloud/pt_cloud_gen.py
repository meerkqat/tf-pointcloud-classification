import math
import json
import os
from numpy import matmul
from random import choice
from random import randint
from random import random
from random import uniform
from numpy import sign
from sys import float_info

from pt_cloud_consts import *


# all generation methods have parameters:
# 	n (number of points to sample)
# 	object specific parameters (e.g. radius, height...)
# 	dt (amount of noise in data = true_point * dt*rand[-1,1] ; so max deviation from true point is sqrt(3)*dt)

"""
Generates a pointcloud of a flat rectangle (flat in z dim). 

@param
n : int : number of points in the resulting pointcloud
w : number : width of rectangle (x dim)
h : number : height of rectangle (y dim)
dt : number : amount of noise for each point - i.e. how far away from its true position each point should be

@return
object[vertex[float, float, float],...] : pointcloud of a flat rectangle
"""
def gen_rect(n, w, h, dt):
	rect = []

	for i in xrange(0,n):
		x = random()*w - w/2.0
		y = random()*h - h/2.0
		rect.append([x + dt*uniform(-1,1), y + dt*uniform(-1,1), dt*uniform(-1,1)])

	return rect

"""
Generates a pointcloud of a cuboid. 

@param
n : int : number of points in the resulting pointcloud
w : number : width of cuboid (x dim)
h : number : height of cuboid (z dim)
d : number : depth of cuboid (y dim)
dt : number : amount of noise for each point - i.e. how far away from its true position each point should be

@return
object[vertex[float, float, float],...] : pointcloud of a cuboid
"""
def gen_box(n, w, h, d, dt):
	box = []

	# welp, this works i guess...
	sides = ["[random()*w-w/2.0, random()*d-d/2.0, h/2.0]",
			 "[random()*w-w/2.0, random()*d-d/2.0, -h/2.0]",
			 "[random()*w-w/2.0, d/2.0, random()*h-h/2.0]",
			 "[random()*w-w/2.0, -d/2.0, random()*h-h/2.0]",
			 "[w/2.0, random()*d-d/2.0, random()*h-h/2.0]",
			 "[-w/2.0, random()*d-d/2.0, random()*h-h/2.0]"]
	for i in xrange(0,n):
		vtx = eval(choice(sides))
		box.append([vtx[0] + dt*uniform(-1,1), vtx[1] + dt*uniform(-1,1), vtx[2] + dt*uniform(-1,1)])

	return box

"""
Generates a pointcloud of a flat circle (flat in z dim). 

@param
n : int : number of points in the resulting pointcloud
r : number : radius of circle
dt : number : amount of noise for each point - i.e. how far away from its true position each point should be

@return
object[vertex[float, float, float],...] : pointcloud of a flat circle
"""
def gen_circle(n, r, dt):
	circle = []

	for i in xrange(0,n):
		t = 2*math.pi*random()
		u = random()*r+random()*r
		u = u if u<r else 2*r-u
		circle.append([u*math.cos(t) + dt*uniform(-1,1), u*math.sin(t) + dt*uniform(-1,1), dt*uniform(-1,1)])

	return circle

"""
Generates a pointcloud of a sphere. 

@param
n : int : number of points in the resulting pointcloud
r : number : radius of sphere
dt : number : amount of noise for each point - i.e. how far away from its true position each point should be

@return
object[vertex[float, float, float],...] : pointcloud of a sphere
"""
def gen_sphere(n, r, dt):
	sphere = []

	for i in xrange(0,n):
		t = 2*math.pi*random()
		u = random()*r+random()*r
		u = u if u<r else u-2*r
		sphere.append([math.sqrt(r*r-u*u)*math.cos(t) + dt*uniform(-1,1), math.sqrt(r*r-u*u)*math.sin(t) + dt*uniform(-1,1), u + dt*uniform(-1,1)])

	return sphere

"""
Generates a pointcloud of a cylinder without the top and bottom faces. 

@param
n : int : number of points in the resulting pointcloud
r : number : radius of cylinder
h : number : height of cylinder (z dim)
dt : number : amount of noise for each point - i.e. how far away from its true position each point should be

@return
object[vertex[float, float, float],...] : pointcloud of cylinder
"""
def gen_tube(n, r, h, dt):
	cylinder = []

	for i in xrange(0,n):
		t = 2*math.pi*random()
		cylinder.append([r*math.cos(t) + dt*uniform(-1,1), r*math.sin(t) + dt*uniform(-1,1), random()*h + dt*uniform(-1,1)])

	return cylinder

"""
def gen_cyliner(n, r, h, dt):
	cylinder = []

	for i in xrange(0,n):
		pass

	return cylinder

def gen_cone(n, r, h, dt):
	cone = []

	for i in xrange(0,n):
		pass

	return cone
"""


"""
Generates a pointcloud of a superquadric

@param
n : int : number of points in the resulting pointcloud
A B C r s t : number : parameters defining the superquadric
dt : number : amount of noise for each point - i.e. how far away from its true position each point should be

@return
object[vertex[float, float, float],...] : pointcloud of a superquadric
"""
def gen_superquad(n, A, B, C, r, s, t, dt):
	superquad = []
	f = lambda w,m: sign(math.sin(w))*pow(abs(math.sin(w)),m)
	g = lambda w,m: sign(math.cos(w))*pow(abs(math.cos(w)),m)
	
	for i in xrange(0,n):
		v = uniform(-math.pi/2, math.pi/2)
		u = uniform(-math.pi, math.pi)
		superquad.append([A*g(v,2./r)*g(u,2./r) + dt*uniform(-1,1), B*g(v,2./s)*f(u,2./s) + dt*uniform(-1,1), C*f(v,2./t) + dt*uniform(-1,1)])

	return superquad


"""
Moves the pointcloud of the object by +(x,y,z).

@param
obj : object[vertex[float,float,float],...] : pointcloud of object to move
x : number : how far to move the object along the x axis
y : number : how far to move the object along the y axis
z : number : how far to move the object along the z axis

@return
object[vertex[float, float, float],...] : moved object
"""
def translate(obj, x, y, z):
	for i in xrange(0,len(obj)):
		obj[i][0] = obj[i][0]+x
		obj[i][1] = obj[i][1]+y
		obj[i][2] = obj[i][2]+z
	return obj

"""
Rotates the pointcloud of the object around the x axis.

@param
obj : object[vertex[float,float,float],...] : pointcloud of object to move
deg : number : how much to rotate the object in degrees

@return
object[vertex[float, float, float],...] : rotated object
"""
def rotate_x(obj, deg):
	th = deg*math.pi/180.0
	rx = [[1,0,0],[0,math.cos(th),-math.sin(th)],[0,math.sin(th),math.cos(th)]]

	for i in xrange(0,len(obj)):
		obj[i] = matmul(rx,obj[i])
	
	return obj

"""
Rotates the pointcloud of the object around the y axis.

@param
obj : object[vertex[float,float,float],...] : pointcloud of object to move
deg : number : how much to rotate the object in degrees

@return
object[vertex[float, float, float],...] : rotated object
"""
def rotate_y(obj, deg):
	th = deg*math.pi/180.0
	ry = [[math.cos(th),0,math.sin(th)],[0,1,0],[-math.sin(th),0,math.cos(th)]]

	for i in xrange(0,len(obj)):
		obj[i] = matmul(ry,obj[i])
	
	return obj

"""
Rotates the pointcloud of the object around the z axis.

@param
obj : object[vertex[float,float,float],...] : pointcloud of object to move
deg : number : how much to rotate the object in degrees

@return
object[vertex[float, float, float],...] : rotated object
"""
def rotate_z(obj, deg):
	th = deg*math.pi/180.0
	rz = [[math.cos(th),-math.sin(th),0],[math.sin(th),math.cos(th),0],[0,0,1]]

	for i in xrange(0,len(obj)):
		obj[i] = matmul(rz,obj[i])
	
	return obj

"""
Rotates the pointcloud of the object around all axes (Rx*Ry*Rz*Obj). 
This is preferred over using multiple separate rotations around x, y and z, since it combines 
all rotational matrices into one and does only one traversal over all the points in the pointcloud.

@param
obj : object[vertex[float,float,float],...] : pointcloud of object to move
ax : number : angle to rotate around x axis in degrees
ay : number : angle to rotate around y axis in degrees
az : number : angle to rotate around z axis in degrees

@return
object[vertex[float, float, float],...] : rotated object
"""
def rotate(obj, ax, ay, az):
	tx = ax*math.pi/180.0
	ty = ay*math.pi/180.0
	tz = az*math.pi/180.0

	rx = [[1,0,0],[0,math.cos(tx),-math.sin(tx)],[0,math.sin(tx),math.cos(tx)]]
	ry = [[math.cos(ty),0,math.sin(ty)],[0,1,0],[-math.sin(ty),0,math.cos(ty)]]
	rz = [[math.cos(tz),-math.sin(tz),0],[math.sin(tz),math.cos(tz),0],[0,0,1]]

	# combine rotation matrices so only one traversal is necessary
	rxy = matmul(rx,ry)
	rxyz = matmul(rxy, rz)

	for i in xrange(0,len(obj)):
		obj[i] = matmul(rxyz,obj[i])
	return obj

"""
Rotates the pointcloud of the object randomly around all axes. 
This is preferred over using three separate random rotations around x, y and z, since it combines 
all rotational matrices into one and does only one traversal over all the points in the pointcloud.

@param
obj : object[vertex[float,float,float],...] : pointcloud of object to move

@return
object[vertex[float, float, float],...] : rotated object
"""
def rand_rotate(obj):
	return rotate(obj, 360*random(), 360*random(), 360*random())

"""
Exports pointcloud of all objects to a .obj file

@param
objects : objects[(object[vertex[float,float,float]...],string)...] : array of objects to export
fname : string : name of the .obj file to export to
"""
def exportObj(objects, fname="scene"):
	f = open(fname+".obj", "w")
	for obj,cls in objects:
		for vtx in obj:
			f.write("v %f %f %f\n" % (vtx[0], vtx[1], vtx[2]))
		f.write("class-"+cls+"\n")
	f.close()


"""
Exports pointcloud of all objects to a .csv file

@param
objects : objects[(object[vertex[float,float,float]...],string)...] : array of objects to export
fname : string : name of the .obj file to export to
"""
def exportCSV(objects, fname="scene"):
	f = open(fname+".csv", "w")
	for obj,cls in objects:
		for vtx in obj:
			f.write("%f,%f,%f\n" % (vtx[0], vtx[1], vtx[2]))
		f.write("class-"+cls+"\n")
	f.close()

"""
Imports point cloud scenes from data folder as 3d array.

@param
data_dir : string : folder where data is located (.obj files)
separate_objects : boolean : if true each object gets a separate array in scene, otherwise they're all lumped together into one big scene array

@return
{string:scene[vertex[float,float,float]...]} : dictionary of imported scenes; keys are scene ids, values are the pointcloud arrays

or if separate_objects is true

{string:scene[(object[vertex[float,float,float]...],string)...]} : dictionary of imported scenes; keys are scene ids, values are the pointcloud arrays
"""
def importObj(data_dir=DATA_DIR, separate_objects=False):
	scenes = {}

	for file in os.listdir(data_dir):
		if file.endswith(".obj"):
			f = open(data_dir+file,"r")

			scene = [[]]
			obj_idx = 0
			
			for line in f:
				if line.startswith("v "):
					banana = line.split(" ")
					scene[obj_idx].append([float(banana[1]),float(banana[2]),float(banana[3])])
				if separate_objects and line.startswith("class"):
					banana = line.split("-")
					scene[obj_idx] = (scene[obj_idx],banana[1])
					obj_idx += 1
					scene.append([])
			
			if not separate_objects:
				scene = scene[0]
			else:
				# remove last appended (empty) obj array
				scene.pop(-1) 

			scenes[file[:-4]] = scene
			f.close()

	return scenes

"""
Generates a specified number of random scenes. 
Saves information about each generated scene into scenes.json

@param
n_scenes : int : number of scenes to generate
mn_objects : int : minimum amount of objects allowed to generate in each scene
mx_objects : int : maximum amount of objects allowed to generate in each scene
mn_points : int : minimum number of points allowed in each object's pointcloud
mx_points : int : maximum number of points allowed in each object's pointcloud
mn_size : number : minimum allowed size of each object
mx_size : number : maximum allowed size of each object
mn_delta : number : minimum amount of noise allowed in each object's pointcloud
mx_delta : number : maximum amount of noise allowed in each object's pointcloud
mn_dist : number : minimum distance allowed to translate each object along each axis after generating it
mx_dist : number : maximum distance allowed to translate each object along each axis after generating it
objects : name[string,...] : list of objects that are allowed to generate in a scene; possible values are "rect", "box", "circle", "sphere", "tube", "superquad"
save_dir : string : where to save all the generated scenes; can be "" to save to current dir, otherwise must be terminated by a "/"
save_csv : boolean : if true also saves all points to a csv file (x,y,z\nx,y,z\n...)

@return
{
	string:
		scene[
			(
				object[vertex[float,float,float]...]
			,
				string
			)...
		]
} : dictionary of generated scenes; keys are scene ids, values are the pointcloud arrays
"""
def generate_scenes(n_scenes, mn_objects, mx_objects, mn_points, mx_points, mn_size, mx_size, mn_delta, mx_delta, mn_dist, mx_dist, objects=["rect","box","circle","sphere","tube"], save_dir="data/", save_csv=False):
	
	print "Generating %d scenes\n\t%d-%d objects per scene\n\t%d-%d points per object \
	\n\t%.2f-%.2f object size\n\t%.2f-%.2f noise\n\t%.2f-%.2f object distance from origin\n\tobjects: %s \
	\n\tsaving to: %s\n\n" % (n_scenes, mn_objects, mx_objects, mn_points, mx_points, mn_size, mx_size, mn_delta, mx_delta, mn_dist, mx_dist, objects, save_dir)
	
	assert_dir(save_dir)
	
	descriptor = {}
	scenes = {}
	
	# generate scenes
	for i in xrange(0, n_scenes):
		scene = []

		scene_id = "scene%04d" % i
		descriptor[scene_id] = {}

		print "Generating",scene_id

		# generate objects for each scene
		n_objects = randint(mn_objects, mx_objects)
		for j in xrange(0,n_objects):
			# gen params
			next_obj = choice(objects)
			n_pts = randint(mn_points, mx_points)
			a = uniform(mn_size, mx_size)
			b = uniform(mn_size, mx_size)
			c = uniform(mn_size, mx_size)
			dt = uniform(mn_delta, mx_delta)

			# high max val can lead to degenerate looking superquads
			r = uniform(float_info.min, 6)
			s = uniform(float_info.min, 6)
			t = uniform(float_info.min, 6)

			tx = uniform(mn_dist, mx_dist)*choice([-1,1])
			ty = uniform(mn_dist, mx_dist)*choice([-1,1])
			tz = uniform(mn_dist, mx_dist)*choice([-1,1])
			rx = 360*random()
			ry = 360*random()
			rz = 360*random()

			obj_id = "obj%04d" % j
			descriptor[scene_id][obj_id] = {}
			descriptor[scene_id][obj_id]["type"] = next_obj
			descriptor[scene_id][obj_id]["n_pts"] = n_pts
			descriptor[scene_id][obj_id]["dt"] = dt
			descriptor[scene_id][obj_id]["rx"] = rx
			descriptor[scene_id][obj_id]["ry"] = ry
			descriptor[scene_id][obj_id]["rz"] = rz
			descriptor[scene_id][obj_id]["tx"] = tx
			descriptor[scene_id][obj_id]["ty"] = ty
			descriptor[scene_id][obj_id]["tz"] = tz

			#gen object pointcloud + descriptors
			obj = []
			if next_obj == "rect":
				obj = gen_rect(n_pts, a, b, dt)
				descriptor[scene_id][obj_id]["w"] = a
				descriptor[scene_id][obj_id]["h"] = b
				next_obj += ","+str(a)+","+str(b)

			elif next_obj == "box":
				obj = gen_box(n_pts, a, b, c, dt)
				descriptor[scene_id][obj_id]["w"] = a
				descriptor[scene_id][obj_id]["h"] = b
				descriptor[scene_id][obj_id]["d"] = c
				next_obj += ","+str(a)+","+str(b)+","+str(c)

			elif next_obj == "circle":
				obj = gen_circle(n_pts, a/2., dt)
				descriptor[scene_id][obj_id]["r"] = a/2.
				next_obj += ","+str(a/2.)

			elif next_obj == "sphere":
				obj = gen_sphere(n_pts, a/2., dt)
				descriptor[scene_id][obj_id]["r"] = a/2.
				next_obj += ","+str(a/2.)

			elif next_obj == "tube":
				obj = gen_tube(n_pts, a/2., b, dt)
				descriptor[scene_id][obj_id]["r"] = a/2.
				descriptor[scene_id][obj_id]["h"] = b
				next_obj += ","+str(a/2.)+","+str(b)

			elif next_obj == "superquad":
				obj = gen_superquad(n_pts, a/2., b/2., c/2., r, s, t, dt)
				descriptor[scene_id][obj_id]["A"] = a/2.
				descriptor[scene_id][obj_id]["B"] = b/2.
				descriptor[scene_id][obj_id]["C"] = c/2.
				descriptor[scene_id][obj_id]["r"] = r
				descriptor[scene_id][obj_id]["s"] = s
				descriptor[scene_id][obj_id]["t"] = t
				next_obj += ","+str(a/2.)+","+str(b/2.)+","+str(c/2.)+","+str(r)+","+str(s)+","+str(t)
			else:
				print "Unknown object: "+next_obj

			# add object to scene
			scene.append((translate(rotate(obj, rx, ry, rz), tx, ty, tz), next_obj))

		# save scene
		exportObj(scene, save_dir+scene_id)
		if save_csv:
			exportCSV(scene, save_dir+scene_id)
		scenes[scene_id] = scene

	print "Saving descriptor"

	# save descriptors
	f = open(save_dir+"scenes.json", "w")
	json.dump(descriptor, f)
	f.close()

	print "Done\n"
			
	return scenes