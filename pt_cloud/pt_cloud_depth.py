import numpy as np
from PIL import Image
from sys import float_info

from pt_cloud_consts import *
import pt_cloud_gen as gen



"""
Takes a pointcloud and generates depth images from it. Six images are generated per scene - 2 projection planes are perpendicular to the x axis
2 are perpendicular to the y axis and 2 to the z axis. The pairs of perpendicular planes are placed at +-pplane_offset in the coordinate system
and are centered on the axis.

@param
scene : scene[(object[vertex[float,float,float]...],string)...] : pointcloud of the scene
pplane_offset : float : distance between coordinate system origin and projection plane
focus_len : float : distance between projection plane and viewpoint
image_w : number : width of projection plane in coordinate system
image_h : number : height of projection plane in coordinate system
bins_w : int : number of "pixels" along the width of the depth image
bins_h : int : number of "pixels" along the height of the depth image
place_wall : number : where to place a "wall" behind the scene - essentially the max value you can have in any given image. 
	If set to a value <= 0, wall will be placed at max float value.
normalize_vals : number : normalize all values to a range between 0 and this number. 
	If it's set to 0 normalization will not occur, if it's less than 0 then the object will be "cut out" 
	i.e. no space behind & in front of the object; the closest to furthest object pixel range 
	will be normalized to a range of 0 to -normalize_val

@return 
depth_images[
	(
		image_h[
			image_w[float...]...
		]
	,
		bboxes[
		(
			bbox[(int,int),(int,int)]
		,
			string
		)...
		]
	)...
] : generated (depth images, bboxes) for a given scene
"""
def generate_depth_images_scene(scene, pplane_offset=PPLANE_OFFSET, focus_len=FOCUS_LEN, image_w=IMAGE_W, image_h=IMAGE_H, bins_w=BINS_W, bins_h=BINS_H, place_wall=WALL_DIST, normalize_vals=255):

	# projection plane parametrizations
	pplane_params = [ np.array((1.,0.,0.,-pplane_offset)),
					np.array((1.,0.,0.,pplane_offset)),
					np.array((0.,1.,0.,-pplane_offset)),
					np.array((0.,1.,0.,pplane_offset)),
					np.array((0.,0.,1.,-pplane_offset)),
					np.array((0.,0.,1.,pplane_offset)) ]

	# MATHS
	# plane: 
	# 	Ax + By + Cz + D = 0
	# 3d line going through p1 = (x1,y1,z1) and p2 = (x2,y2,z2):
	#	x = x1 + at
	#	y = y1 + bt
	#	z = z1 + ct
	# calc a,b,c simply by taking the vector p2-p1 = [a,b,c] (noted below as "ray")
	# intersection of plane and point (you have p1 & the ray => only thing missing is t; smush the eqs together):
	# 	t = -(Ax1 + By1 + Cz1 + D) / (Aa + Bb + Cc)

	depth_images = []
	bin_increment_w = float(image_w)/bins_w
	bin_increment_h = float(image_h)/bins_h

	# iterate over all projection planes
	for plane_param in pplane_params:

		img_accumulator = []
		bbox_accumulator = []

		# set up viewpoint
		viewpoint = plane_param[:3]*(pplane_offset+focus_len)*(np.sign(plane_param[3]))
		viewpoint_ext = np.append(viewpoint,1) # just so we can simplify with a dot product fn

		# used for later calculations, doesn't need to be calculated for each data point
		plane_param[-1] *=-1
		const =  np.dot(plane_param,viewpoint_ext)
		plane_param[-1] *=-1
		
		max_val = place_wall if (place_wall > 0) else float_info.max

		for obj,cls in scene:
			# init depth image with max values; len(depth_image) = bins_h; len(depth_image[0]) = bins_w
			depth_image = np.full((bins_h,bins_w), max_val)

			for pt in obj:
				# ray from viewpoint to datset point
				ray = pt-viewpoint
				t = -const/np.dot(plane_param[:3],ray)

				# point where ray intersects projection plane
				intersect_pt = np.array((viewpoint[0]+ray[0]*t, viewpoint[1]+ray[1]*t, viewpoint[2]+ray[2]*t))
				
				# remove extra coordinate (dist of plane from origin essentially)
				rm_idx = np.argmax(plane_param[:3])
				intersect_pt_trunc = np.delete(intersect_pt, rm_idx)

				# determine which bin the point falls into
				intersect_pt_trunc[0] += image_w/2.0
				intersect_pt_trunc[1] += image_h/2.0
				#check bounds of course
				if (intersect_pt_trunc[0] < 0 or intersect_pt_trunc[0] >= image_w or intersect_pt_trunc[1] < 0 or intersect_pt_trunc[1] >= image_h):
					continue

				bin_idx_w = int(intersect_pt_trunc[0]/bin_increment_w)
				bin_idx_h = int(intersect_pt_trunc[1]/bin_increment_h)

				# distance from intersection to ptcloud point
				dv = intersect_pt-pt
				dist = float((np.dot(dv,dv))**0.5)

				# potentially add point to depth image
				if (depth_image[bin_idx_h][bin_idx_w] > dist):
					depth_image[bin_idx_h][bin_idx_w] = dist

			#find bbox
			margin = 1
			mx_x = 0
			mx_y = 0
			mn_x = len(depth_image[0])
			mn_y = len(depth_image)
			for r in xrange(len(depth_image)):
				for c in xrange(len(depth_image[r])):
					if depth_image[r][c] < max_val:
						if c < mn_x:
							mn_x = c
						if c > mx_x:
							mx_x = c 

						if r < mn_y:
							mn_y = r 
						if r > mx_y:
							mx_y = r

			mx_x = min(mx_x+margin, len(depth_image[0]))
			mx_y = min(mx_y+margin, len(depth_image))
			mn_x = max(mn_x-margin, 0)
			mn_y = max(mn_y-margin, 0)

			# append results
			img_accumulator.append(depth_image)
			bbox_accumulator.append( ([(mn_x,mn_y),(mx_x,mx_y)],cls) )

		depth_image = np.minimum.reduce(img_accumulator)

		# normalize values
		if normalize_vals > 0:
			mx = float(depth_image.max())
			mx = max(mx, float_info.min) # just in case
			depth_images.append( (depth_image*normalize_vals/mx,bbox_accumulator) )
		elif normalize_vals < 0:
			mx = float(depth_image.max())
			depth_image[depth_image == mx] = -1
			mx = float(depth_image.max())
			depth_image[depth_image == -1] = mx+1
			mn = float(depth_image.min())
			depth_image = depth_image-mn
			mx = float(depth_image.max())
			mx = max(mx, float_info.min) # just in case
			depth_images.append( (depth_image*(-normalize_vals)/mx,bbox_accumulator) )

	return depth_images

"""
Exports the given depth image as a .png file.

@param
image : depth_image[float...] : 
fname : string : name of the .png file to export to
"""
def exportDepthImage(image, fname="scene_depth"):
	col_depth = 255
	mx = float(image.max())
	image = image*col_depth/mx # normalize
	image = (image-col_depth)*-1 # invert colors
	# what even is this, why does it need *255, why does it auto convert to [0,1]
	im = Image.fromarray(image.astype('uint8')*col_depth)
	#im.show()
	im.save(fname+".png","PNG")


"""
Returns scenes converted to depth images for all scenes in data directory

@param
pplane_offset : float : distance between coordinate system origin and projection plane
focus_len : float : distance between projection plane and viewpoint
image_w : number : width of projection plane in coordinate system
image_h : number : height of projection plane in coordinate system
bins_w : int : number of "pixels" along the width of the depth image
bins_h : int : number of "pixels" along the height of the depth image
data_dir : string : folder where data is located (.obj files)
export_images : boolean : if true .png of the depth images will also be saved
place_wall : number : where to place a "wall" behind the scene - essentially the max value you can have in any given image. 
	If set to a value <= 0, wall will be placed at max float value.
normalize_vals : number : normalize all values to a range between 0 and this number. 
	If it's set to 0 or less normalization will not occur.
data : {string:scene[(object[vertex[float,float,float]...],string)...]} : dictionary of scene_id:scene. 
	Default is empty dict '{}' in which case it will attempt to read scenes from .obj files in the data directory

@return 
{ 
	string:
		depth_images[
			(
				image_h[
					image_w[float...]...
				]
			,
				bboxes[
				(
					bbox[(int,int),(int,int)]
				,
					string
				)...
				]
			)...
		]
} : dictionary with keys denoting scene id and values being the (depth images, bboxes) for a scene
"""
def generate_depth_images_data(pplane_offset=PPLANE_OFFSET, focus_len=FOCUS_LEN, image_w=IMAGE_W, image_h=IMAGE_H, bins_w=BINS_W, bins_h=BINS_H, data_dir=DATA_DIR, export_images=False, place_wall=WALL_DIST, normalize_vals=255, data={}):
	print "Generating depth images"

	assert_dir(data_dir)

	if len(data) == 0:
		print "Importing scenes"
		data = gen.importObj(data_dir, separate_objects=True)

	depth_images = {}

	ctr = 0
	datalen = len(data.keys())

	for scene_id,scene in data.iteritems():
		ctr += 1
		print "Generating depth_scene %s  %d/%d" % (scene_id,ctr, datalen)
		depth_images[scene_id] = generate_depth_images_scene(scene, pplane_offset, focus_len, image_w, image_h, bins_w, bins_h, place_wall, normalize_vals)
		
	if export_images:
		ctr = 0
		for scene_id,images in depth_images.iteritems():
			ctr += 1
			print "Exporting %s to .png  %d/%d" % (scene_id,ctr, datalen)
			for i in xrange(len(images)):
				exportDepthImage(images[i][0], data_dir+scene_id+"_depth"+str(i))

	print "Done\n" 
	

	return depth_images