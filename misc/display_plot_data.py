import numpy as np 
import matplotlib.pyplot as plt 
import os 
from random import choice

# baseline_1
# baseline_2
# leakyrelu_1
# leakyrelu_2
# batchnorm_1
# batchnorm_2
# convbatchnorm_1
# convbatchnorm_2
# weightreg_1
# weightreg_2
# activreg_1
# activreg_2
# optnadam_1
# optnadam_2
# optrmsprop_1
# optrmsprop_2
# optadagrad_1
# optadagrad_2
# optadadelta_1
# optadadelta_2
# optadamax_1
# optadamax_2
# batch5_1
# batch5_2
# batch20_1
# batch20_2

# baseline
# leakyrelu
# batchnorm
# convbatchnorm
# weightreg
# activreg
# optnadam
# optrmsprop
# optadagrad
# optadadelta
# optadamax
# batch5
# batch20

# batch_loss_history
# batch_xs
# epoch_loss_history
# epoch_xs

plt_data_dir = "data/plots/"

plt.figure(figsize=(12.8,6))

plt.ylim(0,500)

plot_batch =  False
plot_epoch =  True

### what to plot
### keys are folder names without the _#,
### values are plot colors (up to _# colors)
plot_stuff = {
"baseline":["#0008ff","#00037a"],
"leakyrelu":["#ff8c00","#8c4d00"],
# "batchnorm":["r"],
# "convbatchnorm":["r"],
# "weightreg":["r"],
# "activreg":["r"],
"optnadam":["#ff00e1","#870077"],
# "optrmsprop":["r"],
# "optadagrad":["r"],
# "optadadelta":["r"],
# "optadamax":["r"],
# "lessconvflt":["r"],
# "moreconvflt":["r"],
# "moredropout":["r"],
"lessdropout":["#ff002d","#7a0016"],
# "denseparamsup":["r"],
# "denseparamsdown":["r"],
"denser":["#00fffa","#008784"],
# "lessdense":["r"],
# "fullres":["r"],
# "batch5":["r"],
# "batch20":["r"],
#"custom":["r","m"],
#"custom2":["r","m"],#,"g"],
#"custom3":["r"],
#"weightinit":["r","m"],
#"weightregdown":["r"],
"weightregup":["#2aff00","#137700"],
#"lrdecay":["r"],
#"lrdecaysmall":["r"],
#"lrup":["r"],
}

# plot_stuff = {
# "baseline":["b"],

# "leakyrelu":["#"+"".join( [choice("0123456789ABCDEF") for i in xrange(6)] )],
# "batchnorm":["#"+"".join( [choice("0123456789ABCDEF") for i in xrange(6)] )],
# "convbatchnorm":["#"+"".join( [choice("0123456789ABCDEF") for i in xrange(6)] )],
# "weightreg":["#"+"".join( [choice("0123456789ABCDEF") for i in xrange(6)] )],
# "activreg":["#"+"".join( [choice("0123456789ABCDEF") for i in xrange(6)] )],

# "optnadam":["#"+"".join( [choice("0123456789ABCDEF") for i in xrange(6)] )],
# "optrmsprop":["#"+"".join( [choice("0123456789ABCDEF") for i in xrange(6)] )],
# "optadagrad":["#"+"".join( [choice("0123456789ABCDEF") for i in xrange(6)] )],
# "optadadelta":["#"+"".join( [choice("0123456789ABCDEF") for i in xrange(6)] )],
# "optadamax":["#"+"".join( [choice("0123456789ABCDEF") for i in xrange(6)] )],

# "lessconvflt":["#"+"".join( [choice("0123456789ABCDEF") for i in xrange(6)] )],
# "moreconvflt":["#"+"".join( [choice("0123456789ABCDEF") for i in xrange(6)] )],
# "moredropout":["#"+"".join( [choice("0123456789ABCDEF") for i in xrange(6)] )],
# "lessdropout":["#"+"".join( [choice("0123456789ABCDEF") for i in xrange(6)] )],
# "denseparamsup":["#"+"".join( [choice("0123456789ABCDEF") for i in xrange(6)] )],
# "denseparamsdown":["#"+"".join( [choice("0123456789ABCDEF") for i in xrange(6)] )],
# "denser":["#"+"".join( [choice("0123456789ABCDEF") for i in xrange(6)] )],
# "lessdense":["#"+"".join( [choice("0123456789ABCDEF") for i in xrange(6)] )],
# "fullres":["#"+"".join( [choice("0123456789ABCDEF") for i in xrange(6)] )],

# "weightinit":["#"+"".join( [choice("0123456789ABCDEF") for i in xrange(6)] )],
# "weightregdown":["#"+"".join( [choice("0123456789ABCDEF") for i in xrange(6)] )],
# "weightregup":["#"+"".join( [choice("0123456789ABCDEF") for i in xrange(6)] )],
# "lrdecay":["#"+"".join( [choice("0123456789ABCDEF") for i in xrange(6)] )],
# "lrdecaysmall":["#"+"".join( [choice("0123456789ABCDEF") for i in xrange(6)] )],
# "lrup":["#"+"".join( [choice("0123456789ABCDEF") for i in xrange(6)] )],

# "batch5":["#"+"".join( [choice("0123456789ABCDEF") for i in xrange(6)] )],
# "batch20":["#"+"".join( [choice("0123456789ABCDEF") for i in xrange(6)] )],

# "custom":["#"+"".join( [choice("0123456789ABCDEF") for i in xrange(6)] )],
#}


# {folder:[{varname:vals},{varname:vals}]}
plt_data = {}
plt_err = {}
plt_mse = {}
for subdir, dirs, files in os.walk(plt_data_dir):
	for file in files:
		test_name = subdir[subdir.rfind("/")+1:-2]
		if file.endswith(".npz"):
			try:
				plt_data[test_name].append(np.load(os.path.join(subdir, file)))
			except:
				plt_data[test_name] = [np.load(os.path.join(subdir, file))]

		if file.endswith(".txt"):
			f = open(os.path.join(subdir, file), "r")
			avg_err = 0.
			avg_mse = 0.
			ctr = 0
			for line in f:
				if line.startswith("["):
					banana = line.split(" -> ")
					base = np.array(eval(banana[0]))
					pred = np.array(eval(banana[1]))

					avg_err += base-pred
					avg_mse += ((base-pred)**2).mean()
					ctr += 1

			try:
				plt_err[test_name].append(avg_err/ctr)
				plt_mse[test_name].append(avg_mse/ctr)
			except:
				plt_err[test_name] = [avg_err/ctr]
				plt_mse[test_name] = [avg_mse/ctr]
			f.close()

for key,val in plot_stuff.iteritems():
	for i in range(len(val)):
		if plot_batch:
			plt.plot(plt_data[key][i]["batch_xs"], plt_data[key][i]["batch_loss_history"], val[i], linestyle="-", label=key+"-batch")
		if plot_epoch:
			plt.plot(plt_data[key][i]["epoch_xs"], plt_data[key][i]["epoch_loss_history"], val[i], linestyle="--", label=key+"-epoch")
			#plt.plot(range(1,len(plt_data[key][i]["epoch_xs"])+1), plt_data[key][i]["epoch_loss_history"], val[i], linestyle="--", label=key+"-epoch")

plt.legend()

# plotting this only makes sense if only 1 param is being estimated
labels = []
values = []
for key,val in plt_err.iteritems():
	labels.append(key)
	s = val[0]
	for i in range(1,len(val)):
		s += val[i]
	s = s/(i+1.)

	s = abs(s[0])
	values.append(s)

fig, ax = plt.subplots()
ax.bar(range(len(values)),values)
ax.set_xticks(range(len(values)))
ax.set_xticklabels(labels, rotation='vertical')
fig.tight_layout()


plt_errs = []
for errs in plt_mse.values():
	s = errs[0]
	for i in range(1,len(errs)):
		s += errs[i]
	plt_errs.append(s/(i+1.))

fig, ax = plt.subplots()
ax.bar(range(len(plt_err)),plt_errs)
ax.set_xticks(range(len(plt_err)))
ax.set_xticklabels(plt_err.keys(), rotation='vertical')
fig.tight_layout()


plt.show()