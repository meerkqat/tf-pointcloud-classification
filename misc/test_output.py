import keras
from keras import layers
from keras.models import Sequential, model_from_json, Model
from keras.layers import Activation, Dense, Dropout, Flatten, Input
from keras.layers import Conv2D, MaxPooling2D, LocallyConnected2D
from keras.layers.normalization import BatchNormalization
from keras.optimizers import Adam, Nadam, RMSprop
from keras import regularizers
from keras.initializers import RandomUniform, RandomNormal
import keras.backend as K
from keras.utils import generic_utils

import os
os.environ['TF_CPP_MIN_LOG_LEVEL']='2'

import tensorflow as tf
import numpy as np
import json
import matplotlib.pyplot as plt
from itertools import cycle
from time import sleep

from quiver_engine import server
from keras import backend as K
from keras.layers.advanced_activations import LeakyReLU, PReLU

import sys
sys.path.insert(0, '../pt_cloud')
sys.path.insert(0, '..')
from pt_cloud_consts import *

import models
import data_loader
import rpn_utils

anchor_scales = [128, 256, 512]
anchor_shapes = [[1, 1], [1, 2], [2, 1]]
img_rows = BINS_H
img_cols = BINS_W
train_param_csv = "../test/scenes.h5"
train_bbox_csv = "../test/scenes_bboxes.csv"

x_train = data_loader.load_h5_imgparam_dataset(train_param_csv, img_rows, img_cols)
bbox_train = data_loader.load_bboxes_labels(train_bbox_csv)
input_shape = (img_rows, img_cols, 1)
train = zip(x_train, bbox_train)

datagen = data_loader.data_generator(train, anchor_scales, anchor_shapes)

img_input = Input(shape=input_shape)
x = models.squeezenet(img_input)
out = models.rpn(x, len(anchor_scales)*len(anchor_shapes))
#x_cls,x_reg = models.rpn(x, len(anchor_scales)*len(anchor_shapes))

#model = Model(img_input, x)
model = Model(img_input, out)
opt = Adam(lr=0.001, beta_1=0.9, beta_2=0.999, epsilon=1e-08, decay=0.0)
model.compile(loss='mean_squared_error', optimizer=opt, metrics=['mean_squared_error'])

X,Y,params = datagen.next()
print "in shape:",X.shape
pred_rpn = model.predict_on_batch([X])
try:
	print "out shape:",pred_rpn.shape
except:
	for thing in pred_rpn:
		print "out shape:",thing.shape
