#!/bin/bash

# to capture stdout from a certain command run the following
# ./logger.sh "command to run"
# set MAX_LINES and log FILE names below
# guaranteed to save at least the last MAX_LINES + up to additional MAX_LINES of stdout output from command

MAX_LINES=2000
FILE_1="logger_tmp1.txt"
FILE_2="logger_tmp2.txt"
FILE_FINAL="logger_out.txt"


FILE_CURR=$FILE_1
CTR=1

# run command and pipe to logger
$1 |
	while IFS= read -r line
	do
		# dunp line to stdout as well as to file
		echo $line | tee -a $FILE_CURR

		# roll logs
		if (($CTR >= MAX_LINES))
		then
			# reset ctr
			CTR=0
			# swap prev & curr log
			if [ $FILE_CURR == $FILE_1 ]
			then
				FILE_CURR=$FILE_2
			else
				FILE_CURR=$FILE_1
			fi

			# rm previous log (now current log)
			if [[ -e $FILE_CURR ]]
			then
				rm $FILE_CURR
			fi
		fi

		# increment
		((CTR++))
	done


# determine concat order
if [ $FILE_CURR == $FILE_1 ]
then
	FILE_PREV=$FILE_2
else
	FILE_PREV=$FILE_1
fi

# check if we even had enough input to fill more than 1 log (prev log exists)
if [[ -e $FILE_PREV ]]
then
	cat $FILE_PREV $FILE_CURR > $FILE_FINAL
else
	mv $FILE_CURR $FILE_FINAL
fi

# remove temp logs
if [[ -e $FILE_1 ]]
then
	rm $FILE_1 
fi
if [[ -e $FILE_2 ]]
then
	rm $FILE_2
fi