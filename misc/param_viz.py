import numpy as np
import matplotlib.pyplot as plt
from matplotlib.widgets import Slider, Button, RadioButtons
import math

fig,ax = plt.subplots()
plt.subplots_adjust(bottom=0.3)

plt.axis('equal')

plt.xlim(-100,500)
plt.ylim(-500,500)

r = 150.
off = 200.
f = 10.

a = off-f
k = math.sqrt((r*r)/(a*a-r*r))
n = -k*a
yp = k*(off)+n
xr = (-2*k*n)/(2*(1+k*k))
yr = math.sqrt(r*r - xr*xr)
print "proj plane size:",str(2*yp)
circle = plt.Circle((0, 0), r, color='c')
ax.plot([xr,a,off],[-yr,0,yp], "b")
ax.plot([xr,a,off],[yr,0,-yp], "b")
ax.add_artist(circle)
ax.plot([off,off],[-r,r])

axr = plt.axes([0.25, 0.1, 0.65, 0.03])
axoff = plt.axes([0.25, 0.15, 0.65, 0.03])
axf = plt.axes([0.25, 0.2, 0.65, 0.03])

sr = Slider(axr, 'r', 1, 500, valinit=r)
soff = Slider(axoff, 'offset', 1, 500, valinit=off)
sf = Slider(axf, 'focal len', -500, 500, valinit=f)

def update(val):
	try:
		r = sr.val
		off = soff.val
		f = sf.val
		a = off-f
		k = math.sqrt((r*r)/(a*a-r*r))
		n = -k*a
		yp = k*(off)+n
		xr = (-2*k*n)/(2*(1+k*k))
		yr = math.sqrt(r*r - xr*xr)
		print "proj plane size:",str(2*yp)
		circle = plt.Circle((0, 0), r, color='c')
		ax.clear()
		ax.set_xlim([-100,500])
		ax.set_ylim([-500,500])
		ax.plot([xr,a,off],[-yr,0,yp], "b")
		ax.plot([xr,a,off],[yr,0,-yp], "b")
		ax.add_artist(circle)
		ax.plot([off,off],[-r,r])
	except:
		print "Inappropriate ranges"

sr.on_changed(update)
soff.on_changed(update)
sf.on_changed(update)

plt.show()