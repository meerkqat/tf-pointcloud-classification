from Tkinter import *
import numpy as np

scale = 21
width = 30
height = 30
MIN_DRAG = 0.001

tick_len = scale/3
center_sz = scale/6

sw = scale*width
sh = scale*height

sw2 = scale*width/2
sh2 = scale*height/2

gtcx = 0
gtcy = 0
gtw = 4
gth = 4
cx = 0
cy = 0
w = 4
h = 4

snap_to = False

# black = in code iterates through all cartesian coords from 0,0 to cols,rows
# blue = rpn regression bboxes
# red = application of regression on cartesian coords
# in rpn_utils.py; apply_regr_np; rpn_to_roi
def apply_regr_np(x,y,w,h,tx,ty,tw,th):
	cx = x + w/2.
	cy = y + h/2.
	cx1 = tx * w + cx
	cy1 = ty * h + cy

	w1 = np.exp(tw) * w
	h1 = np.exp(th) * h
	x1 = cx1 - w1/2.
	y1 = cy1 - h1/2.

	x1 = np.round(x1)
	y1 = np.round(y1)
	w1 = np.round(w1)
	h1 = np.round(h1)
	return x1, y1, w1, h1

def apply_regr_mod(x,y,w,h,tx,ty,tw,th):
	tx = tx-x
	ty = ty-y
	tw = tw-w
	th = th-h
	cx = x + w/2.
	cy = y + h/2.
	cx1 = tx * w + cx
	cy1 = ty * h + cy

	w1 = np.exp(tw) * w
	h1 = np.exp(th) * h
	x1 = cx1 - w1/2.
	y1 = cy1 - h1/2.

	x1 = np.round(x1)
	y1 = np.round(y1)
	w1 = np.round(w1)
	h1 = np.round(h1)
	return x1, y1, w1, h1

# black = ground truth ~ right click to change c; drag to change w,h
# blue = prediction ~ left click to change c; drag to change w,h
# red = error (tx, ty, tw, th)
# in rpn_utils.py; calc_parametrization_labels
def error_fn(gtcx, gtcy, gtw, gth, cx, cy, w, h):
	tx = (gtcx - cx) / float(w)
	ty = (gtcy - cy) / float(h)
	tw = np.log(gtw / float(w))
	th = np.log(gth / float(h))

	return tx,ty,tw,th




t_function = error_fn
#t_function = apply_regr_np
#t_function = apply_regr_mod




def snap_toggle(event):
	global snap_to
	snap_to = not snap_to
	print "Snapping:",snap_to

def lclick(event):
	global cx, cy, prev_mousex, prev_mousey
	if snap_to:
		cx = round((event.x-sw2)/float(scale))
		cy = round((event.y-sh2)/float(scale))
	else :	
		cx = (event.x-sw2)/float(scale)
		cy = (event.y-sh2)/float(scale)

	#print cx,cy
	canvas.delete("all")
	render(t_function)

def rclick(event):
	global gtcx, gtcy, prev_gtmousex, prev_gtmousey
	if snap_to:
		gtcx = round((event.x-sw2)/float(scale))
		gtcy = round((event.y-sh2)/float(scale))
	else:
		gtcx = (event.x-sw2)/float(scale)
		gtcy = (event.y-sh2)/float(scale)

	#print gtcx,gtcy
	canvas.delete("all")
	render(t_function)


def ldrag(event):
	global w, h
	startx = (cx*scale)+sw2
	starty = (cy*scale)+sh2

	full_dx = (event.x-startx)/float(scale)
	full_dy = (event.y-starty)/float(scale)

	if snap_to:
		full_dx = round(full_dx)
		full_dy = round(full_dy)
	
	w = max(MIN_DRAG,full_dx)
	h = max(MIN_DRAG,full_dy)	

	canvas.delete("all")
	render(t_function)

def rdrag(event):
	global gtw, gth
	startx = (gtcx*scale)+sw2
	starty = (gtcy*scale)+sh2

	full_dx = (event.x-startx)/float(scale)
	full_dy = (event.y-starty)/float(scale)

	if snap_to:
		full_dx = round(full_dx)
		full_dy = round(full_dy)

	gtw = max(MIN_DRAG,full_dx)
	gth = max(MIN_DRAG,full_dy)

	canvas.delete("all")
	render(t_function)

def render_axes():
	canvas.create_line(sw2, 0, sw2, sh)
	canvas.create_line(0, sh2, sw, sh2)

	for i in range(scale,sw,scale):
		canvas.create_line(i, sh2-tick_len, i, sh2+tick_len)
	for i in range(scale,sh,scale):
		canvas.create_line(sw2-tick_len, i, sw2+tick_len,i)

def render(t_fn):
	render_axes()
	x1 = ((gtcx-gtw/2.)*scale)+sw2
	x2 = ((gtcx+gtw/2.)*scale)+sw2
	y1 = ((gtcy-gth/2.)*scale)+sh2
	y2 = ((gtcy+gth/2.)*scale)+sh2
	c1 = (gtcx*scale)+sw2
	c2 = (gtcy*scale)+sh2
	canvas.create_rectangle(x1,y1,x2,y2, outline="black")
	canvas.create_oval(c1-center_sz, c2-center_sz, c1+center_sz, c2+center_sz, fill="black")

	x1 = ((cx-w/2.)*scale)+sw2
	x2 = ((cx+w/2.)*scale)+sw2
	y1 = ((cy-h/2.)*scale)+sh2
	y2 = ((cy+h/2.)*scale)+sh2
	c1 = (cx*scale)+sw2
	c2 = (cy*scale)+sh2
	canvas.create_rectangle(x1,y1,x2,y2, outline="blue")
	canvas.create_oval(c1-center_sz, c2-center_sz, c1+center_sz, c2+center_sz, fill="blue")

	#tx,ty,tw,th = error_fn(gtcx, gtcy, gtw, gth, cx, cy, w, h)
	#tx,ty,tw,th = apply_regr_np(gtcx, gtcy, gtw, gth, cx, cy, w, h)
	tx,ty,tw,th = t_fn(gtcx, gtcy, gtw, gth, cx, cy, w, h)

	x1 = ((tx-tw/2.)*scale)+sw2
	x2 = ((tx+tw/2.)*scale)+sw2
	y1 = ((ty-th/2.)*scale)+sh2
	y2 = ((ty+th/2.)*scale)+sh2
	c1 = (tx*scale)+sw2
	c2 = (ty*scale)+sh2
	canvas.create_rectangle(x1,y1,x2,y2, outline="red")
	canvas.create_oval(c1-center_sz, c2-center_sz, c1+center_sz, c2+center_sz, fill="red")

master = Tk()

canvas = Canvas(master, width=sw, height=sh)
canvas.pack()

render(t_function)

canvas.bind("<Button-1>", lclick)
canvas.bind("<Button-3>", rclick)
canvas.bind("<B1-Motion>", ldrag)
canvas.bind("<B3-Motion>", rdrag)
canvas.bind("<Button-2>", snap_toggle)

mainloop()