import h5py
import numpy as np 
import matplotlib.pyplot as plt 
import os 
from random import choice
from math import log
from sys import float_info

def logmap(arr):
	return map(lambda x: log(min(0.,x)+float_info.min),arr)

def meanify(arr):
	ret = []
	for i in xrange(len(arr)):
		ret.append(np.nanmean(arr[:i+1]))
	return ret

def replace_nan(arr):
	arr = np.array(arr)
	if np.isnan(arr[0]):
		arr[0] = arr[1]
	for i in xrange(1,len(arr)):
		if np.isnan(arr[i]):
			arr[i] = arr[i-1]
	return arr

def remove_nan(arr):
	arr = np.array(arr)
	return arr[np.logical_not(np.isnan(arr))]

def cut(arr, lim):
	return arr[:lim]

max_run = 6000

fname = "loss_acc170922_092142.h5"

loss_acc = []
with h5py.File(fname, 'r') as hf:
	loss_acc = hf['loss_acc'][:]

print loss_acc.shape

# transpose data (group respective losses & acc in arrays together)
plt_data = zip(*loss_acc)
plt_x = range(0,len(loss_acc))

# plt.figure(figsize=(12.8,6))
# plt.ylim(0,500)

print np.nanmean(plt_data[4])

#plt_data = map(logmap,plt_data)
#plt_data = map(remove_nan,plt_data)

#plt_data = map(lambda a:cut(a,max_run), plt_data)
plt_data = map(replace_nan,plt_data)
plt_data = map(meanify,plt_data)

plt.subplot(211)
plt.plot(plt_x[:max_run], plt_data[0][:max_run], "b", linestyle="-", label="rpn_cls_loss")
plt.plot(plt_x[:max_run], plt_data[1][:max_run], "c", linestyle="-", label="rpn_reg_loss")
plt.plot(plt_x[:max_run], plt_data[2][:max_run], "r", linestyle="-", label="param_reg_loss")

plt.legend()

plt.subplot(212)
plt.plot(plt_x[:max_run], plt_data[3][:max_run], "b", linestyle="-", label="box_acc")
plt.plot(plt_x[:max_run], plt_data[4][:max_run], "r", linestyle="-", label="param_acc")

plt.legend()

plt.show()
